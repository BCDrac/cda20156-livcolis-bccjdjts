<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
		<title>Gestion colis</title>
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
	
	<body>

		<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des colis.</p>
		
		<h1>Gestion colis</h1>
				
		<div >
			<figure>
				<a href="${pageContext.request.contextPath}/parcelRegister">
					<img src="${pageContext.request.contextPath}/resources/img/parcelManage.png"
		         	alt="Gestion de colis">
		       	 </a>
	  			<figcaption>Créer un nouveau colis</figcaption>
 			</figure>
 			<figure>
				<a href="${pageContext.request.contextPath}/updateParcel">
					<img src="${pageContext.request.contextPath}/resources/img/update.png"
		         	alt="Gestion de colis">
		       	 </a>
	  			<figcaption>Mettre à jour un colis</figcaption>
 			</figure>
 			<figure>
				<a href="${pageContext.request.contextPath}/barCodeParcel">
					<img src="${pageContext.request.contextPath}/resources/img/search.png"
		         	alt="Gestion de colis">
		       	 </a>
	  			<figcaption>Afficher les colis</figcaption>
 			</figure>
 			
 			
		<br/>
		<button onclick="self.location.href='delivererDashboard'">Retour</button>
</body>
</html>