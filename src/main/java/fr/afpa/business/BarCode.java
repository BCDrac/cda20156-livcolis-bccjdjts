package fr.afpa.business;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class BarCode {
	
	/**
	 * @author Djé
	 * Cette méthode qui implique l'ajout de la dépendance ZXing permet de générer un code barre depuis une matrice de bits
	 * qui a besoin d'un String en paramètre, le format de code barre souhaité, la largeur et la hauteur. La matrice est ensuite parsée en flux et retournée
	 * 
	 * @param text le paramètre que l'on veut, en l'occurence le code barre du colis
	 * @param width largeur du code barre
	 * @param height hauteur du code barre
	 * @return tableau de bytes
	 */
	public static byte [] getBarCodeImage(String text, int width, int height) {
		try {
			Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<>();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
			Writer writer = new Code128Writer();
			BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.CODE_128, width, height);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
			return byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
		
			
	}

}
