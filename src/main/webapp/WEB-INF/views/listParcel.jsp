<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste colis</title>
</head>

<jsp:include page="structure/header.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>

<c:choose>
		<c:when test="${ not empty listParcels  }">
			<TABLE id="table" BORDER="1">
				<CAPTION>Liste des colis</CAPTION>
				<TR>
					<TH>ID colis</TH>
					<TH>Poids</TH>
					<TH>Prix</TH>
					<TH>Code barre</TH>
				</TR>
				<c:forEach items="${ listParcels }" var="parcel">
					<tr>
						<td><c:out value="${parcel.id}" /></td>
						<td><c:out value="${parcel.weight}" /></td>
						<td><c:out value="${parcel.price}" /></td>
						<td>
							<img src="${pageContext.request.contextPath}/barCodeParcel/${ parcel.code }" width="200" height="100">
						</td>
					</tr>
				</c:forEach>
			</TABLE>
		</c:when>
		<c:when test="${ empty listParcels  }">
			<h2>Aucun colis pour l'instant</h2>
		</c:when>
	</c:choose>
	<a href="${pageContext.request.contextPath}/addParcel">Ajouter un
		colis</a>
	<a href="${pageContext.request.contextPath}/parcelDashboard">Retour tableau de bord</a>
</body>
</html>