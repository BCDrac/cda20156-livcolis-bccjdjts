<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste clients</title>
</head>

<jsp:include page="structure/header.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>
<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des clients.</p>
	<c:choose>
		<c:when test="${ not empty listClients  }">
			<form action="${pageContext.request.contextPath}/searchClient" method="post">
				<input type="text" placeholder="Chercher un client par email" name="email" required />
				<br>
				<c:out value="${ errorSearchEmail }"></c:out>
				<br />
				<button type="submit">Valider</button>
				<br />
			</form>
			<TABLE id="table" BORDER="1">
				<CAPTION>Liste des clients</CAPTION>
				<TR>
					<TH>Numéro client</TH>
					<TH>Nom</TH>
					<TH>Prénom</TH>
					<TH>Email</TH>
					<TH>Téléphone</TH>
					<TH>Supprimer</TH>
					<TH>Modifier</TH>
				</TR>
				<c:forEach items="${ listClients }" var="client">
					<tr>
						<td><c:out value="${client.id}" /></td>
						<td><c:out value="${client.lastName}" /></td>
						<td><c:out value="${client.firstName}" /></td>
						<td><c:out value="${client.email}" /></td>
						<td><c:out value="${client.phone}" /></td>
						<td><a
							href="${pageContext.request.contextPath}/deleteClient/${client.id }">Supprimer</a></td>
						<td><a
							href="${pageContext.request.contextPath}/editClient/${client.id }">Modifier</a></td>
					</tr>
				</c:forEach>
			</TABLE>
		</c:when>
		<c:when test="${ empty listClients  }">
			<h2>Aucun client pour l'instant</h2>
		</c:when>
	</c:choose>
	<a href="${pageContext.request.contextPath}/addClient">Ajouter un
		client</a>
	<a href="${pageContext.request.contextPath}/backtodashboard">Retour tableau de bord</a>
</body>
</html>