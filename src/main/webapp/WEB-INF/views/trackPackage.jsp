<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Suivi colis</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/trackParcel.css">
</head>

	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>

	<c:choose>
		<c:when test="${ not empty parcel }">
			<div class="container-outer">
				<div class="container-inner">
					<TABLE id="table" >
						<CAPTION>Suivi de votre colis n° <c:out value="${ parcel.code }"/></CAPTION>
						<c:forEach items="${ deliveryList }" var="delivery" varStatus="status" >
							<tr>
								<td><c:choose>
										<c:when test="${ delivery.done == false }">
											<img
												src="${pageContext.request.contextPath}/resources/img/next_relay_point.png">
										</c:when>
										<c:when test="${ delivery.done == true }">
											<img
												src="${pageContext.request.contextPath}/resources/img/current_relay_point.png">
												<c:choose>
													<c:when test="${ delivery.step == 1 }">
														<br>
														<h4>Votre colis est expédié</h4>
													</c:when>
													<c:when test="${ status.last }">
														<h4>Votre colis est arrivé au point relais final. Vous pouvez le récupérer</h4>
													</c:when>
												</c:choose>
										</c:when>
									</c:choose> <br> <c:out value="${ delivery.relayPoint.name }" />
									<br> <c:out value="${ delivery.date }" />
									</td>
							</tr>
						</c:forEach>
					</TABLE>
				</div>
			</div>
		</c:when>
		<c:when test="${ empty parcel }">
			<h2>Le code de colis est incorrect ou n'existe pas</h2>
		</c:when>
	</c:choose>
	<a href="home">Retour</a>
</body>
</html>