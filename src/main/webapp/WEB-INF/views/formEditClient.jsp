<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modification utilisateur</title>
</head>

	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>
<header>
	<img src="${pageContext.request.contextPath}/resources/img/logo.png">
</header>
<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des colis.</p>
<c:set var = "identificatorClient" value = "${ client.id }" ></c:set>
<h1>Modification du client n° <c:out value="${ identificatorClient }"></c:out></h1>
	<br>
	<br>
	<div>
		<form action="${pageContext.request.contextPath}/editClient" method="post">
			<input type="hidden" name="id" value="${ client.id }" />
			Nom : <input type="text" name="lastName" value="${client.lastName }" required /> <br>
			Prénom : <input type="text" name="firstName" value="${ client.firstName }" required />
			<c:out value="${ listFormatErrors['namesKo'] }"></c:out>
			<br> 
			Email : <input type="text" name="email" value="${ client.email }" required />
			<c:out value="${ listFormatErrors['mailKo'] }"></c:out>
			<c:out value="${ uniqueException }"></c:out>
			<br> 
			Téléphone : <input type="text" name="phone" value="${ client.phone }" required />
			<c:out value="${ listFormatErrors['phoneKo'] }"></c:out>
			<br>
			<br>
			<button type="submit">Valider</button>
		</form>	
	</div>
	<a href="clientList">Retour</a>
</body>
</html>