package fr.afpa.beans.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Deliverer {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliverer_id_gen")
	@SequenceGenerator(name = "deliverer_id_gen", sequenceName = "sequ_deliverer", allocationSize = 1)
	private Long id;
	@Column(nullable = false)
	private String lastName;
	@Column(nullable = false)
	private String firstName;
	@Column(nullable = false, unique = true)
	private String email;
	private String phone;
	@Column(nullable = false)
	private String siret;

	@OneToMany(mappedBy = "deliverer")
	private List<Delivery> deliveryList;

	@OneToOne(mappedBy = "deliverer", cascade = { CascadeType.REMOVE })
	private Login login;

}