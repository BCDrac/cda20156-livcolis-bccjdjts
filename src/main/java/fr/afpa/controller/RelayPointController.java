package fr.afpa.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.dao.Client;
import fr.afpa.beans.dao.RelayPoint;
import fr.afpa.beans.dao.Schedule;
import fr.afpa.business.RelayPointBusiness;
import fr.afpa.business.ScheduleBusiness;

@Controller
public class RelayPointController {

	@Autowired
	private RelayPointBusiness relayPointBusiness;
	
	ArrayList<RelayPoint> listRelayPoint = null;
	
	

	@RequestMapping(value = "/relayPointRegister", method = RequestMethod.GET)
	public String redirectToRegisterForm() {
		return "relayPointRegister";
	}
	
	
	/**
	 * Redirection vers la page de choix concernant les points relais
	 * 
	 * @return ModelAndView : le modèle qui comportera les objets et url
	 * 
	 * @author Cecile
	 */
	@RequestMapping(value = "/relayPointManage", method = RequestMethod.GET)
	public ModelAndView redirectToRelayPointManage(ModelAndView modelView) {
		modelView.setViewName("relayPointManage");
		return modelView;
	}
	
	/**	
	 * méthode de verification d'existance d'un point relais dans la bdd 
	 * @author Sofiane Tayeb
	 * @param name: le nom du point relais à verifier
	 * @return model : comportant le point relais s'il est bien trouvé, un null sinon
	 */
	
	@PostMapping("/verifRelayPoint")
	public ModelAndView verifRelayPoint(ModelAndView model, @RequestParam(value="nameRelayPoint") String name) {
		RelayPoint relay=relayPointBusiness.findRelayPointByName(name);
		model.addObject("relayPoint", relay);
		model.setViewName("parcelRegister");
		return model;
	}
	
	/**Ajout d'un nouveau Point-Relais
	 * 
	 * Récupère les informations du formulaire "relayPointRegister", les vérifie et les enregistre dans la base de données en cas de succès. 
	 * 
	 * @Author : Ju
	 * @param name Le nom du point relais
	 * @param streetNumber Le numéro de la rue
	 * @param streetName Le nom de la rue
	 * @param complement Complément d'adresse éventuel
	 * @param city La ville
	 * @param postalCode Le code postal
	 * @param country Le pays
	 * @param phone Le numéro de téléphone
	 * @param MondayAmOpen Heure d'ouverture du lundi matin
	 * @param MondayAmClose Heure de fermeture du lundi matin
	 * @param MondayPmOpen Heure d'ouverture du lundi après-midi
	 * @param MondayPmClose Heure de fermeture du lundi après-midi
	 * @param TuesdayAmOpen Heure d'ouverture du mardi matin
	 * @param TuesdayAmClose Heure de fermeture du mardi matin
	 * @param TuesdayPmOpen Heure d'ouverture du mardi après-midi
	 * @param TuesdayPmClose Heure de fermeture du mardi après-midi
	 * @param WednesdayAmOpen Heure d'ouverture du mercredi matin
	 * @param WednesdayAmClose Heure de fermeture du mercredi matin
	 * @param WednesdayPmOpen Heure d'ouverture du mercredi après-midi
	 * @param WednesdayPmClose Heure de fermeture du mercredi après-midi
	 * @param ThursdayAmOpen Heure d'ouverture du jeudi matin
	 * @param ThursdayAmClose Heure de fermeture du jeudi matin
	 * @param ThursdayPmOpen Heure d'ouverture du jeudi après-midi
	 * @param ThursdayPmClose Heure de fermeture du jeudi après-midi
	 * @param FridayAmOpen Heure d'ouverture du vendredi matin
	 * @param FridayAmClose Heure de fermeture du vendredi matin
	 * @param FridayPmOpen Heure d'ouverture du vendredi après-midi
	 * @param FridayPmClose Heure de fermeture du vendredi après-midi
	 * @param SaturdayAmOpen Heure d'ouverture du samedi matin
	 * @param SaturdayAmClose Heure de fermeture du samedi matin
	 * @param SaturdayPmOpen Heure d'ouverture du samedi après-midi
	 * @param SaturdayPmClose Heure de fermeture du samedi après-midi
	 * @param SundayAmOpen Heure d'ouverture du dimanche matin
	 * @param SundayAmClose Heure de fermeture du dimanche matin
	 * @param SundayPmOpen Heure d'ouverture du dimanche après-midi
	 * @param SundayPmClose Heure de fermeture du dimanche après-midi
	 * @param modelView Le modèle qui comportera les objets et url
	 * @return String : page à afficher
	 */
	@RequestMapping(value = "/insertRelayPoint", method = RequestMethod.POST)
	public String insertRelayPoint(
		
			@RequestParam(value = "name") String name,
			@RequestParam(value = "streetNumber") String streetNumber, 
			@RequestParam(value = "streetName") String streetName,
			@RequestParam(value = "complement") String complement, 
			@RequestParam(value = "city") String city,
			@RequestParam(value = "postalCode") String postalCode,
			@RequestParam(value = "country") String country, 
			@RequestParam(value = "phone") String phone,
			
			@RequestParam(value = "MondayAmOpen") String MondayAmOpen,
			@RequestParam(value = "MondayAmClose") String MondayAmClose,
			@RequestParam(value = "MondayPmOpen") String MondayPmOpen,
			@RequestParam(value = "MondayPmClose") String MondayPmClose,
			
			@RequestParam(value = "TuesdayAmOpen") String TuesdayAmOpen,
			@RequestParam(value = "TuesdayAmClose") String TuesdayAmClose,
			@RequestParam(value = "TuesdayPmOpen") String TuesdayPmOpen,
			@RequestParam(value = "TuesdayPmClose") String TuesdayPmClose,
			
			@RequestParam(value = "WednesdayAmOpen") String WednesdayAmOpen,
			@RequestParam(value = "WednesdayAmClose") String WednesdayAmClose,
			@RequestParam(value = "WednesdayPmOpen") String WednesdayPmOpen,
			@RequestParam(value = "WednesdayPmClose") String WednesdayPmClose,
			
			@RequestParam(value = "ThursdayAmOpen") String ThursdayAmOpen,
			@RequestParam(value = "ThursdayAmClose") String ThursdayAmClose,
			@RequestParam(value = "ThursdayPmOpen") String ThursdayPmOpen,
			@RequestParam(value = "ThursdayPmClose") String ThursdayPmClose,
			
			@RequestParam(value = "FridayAmOpen") String FridayAmOpen,
			@RequestParam(value = "FridayAmClose") String FridayAmClose,
			@RequestParam(value = "FridayPmOpen") String FridayPmOpen,
			@RequestParam(value = "FridayPmClose") String FridayPmClose,
			
			@RequestParam(value = "SaturdayAmOpen") String SaturdayAmOpen,
			@RequestParam(value = "SaturdayAmClose") String SaturdayAmClose,
			@RequestParam(value = "SaturdayPmOpen") String SaturdayPmOpen,
			@RequestParam(value = "SaturdayPmClose") String SaturdayPmClose,
			
			@RequestParam(value = "SundayAmOpen") String SundayAmOpen,
			@RequestParam(value = "SundayAmClose") String SundayAmClose,
			@RequestParam(value = "SundayPmOpen") String SundayPmOpen,
			@RequestParam(value = "SundayPmClose") String SundayPmClose,
			ModelAndView modelView) {				
		
		
		ScheduleBusiness scMonday = new ScheduleBusiness();	
		ScheduleBusiness scTuesday = new ScheduleBusiness();
		ScheduleBusiness scWednesday = new ScheduleBusiness();
		ScheduleBusiness scThursday = new ScheduleBusiness();
		ScheduleBusiness scFriday = new ScheduleBusiness();
		ScheduleBusiness scSaturday = new ScheduleBusiness();			
		ScheduleBusiness scSunday = new ScheduleBusiness();	
		
		RelayPoint rp= new RelayPoint();
		rp.setName(name);
		rp.setStreetNumber(streetNumber);
		rp.setStreetName(streetName);
		rp.setComplement(complement);
		rp.setCity(city);
		rp.setPostalCode(postalCode);
		rp.setCountry(country);
		rp.setPhone(phone);
		
		ArrayList<Schedule> schedule= new ArrayList <Schedule>();
		schedule.add(scMonday.NewInstanceSchedule("Lundi", MondayAmOpen, MondayAmClose, MondayPmOpen, MondayPmClose, rp ));
		schedule.add(scTuesday.NewInstanceSchedule("Mardi", TuesdayAmOpen, TuesdayAmClose, TuesdayPmOpen, TuesdayPmClose, rp ));
		schedule.add(scWednesday.NewInstanceSchedule("Mercredi", WednesdayAmOpen, WednesdayAmClose, WednesdayPmOpen, WednesdayPmClose, rp ));
		schedule.add(scThursday.NewInstanceSchedule("Jeudi", ThursdayAmOpen, ThursdayAmClose, ThursdayPmOpen, ThursdayPmClose, rp ));
		schedule.add(scFriday.NewInstanceSchedule("Vendredi", FridayAmOpen, FridayAmClose, FridayPmOpen, FridayPmClose, rp ));
		schedule.add(scSaturday.NewInstanceSchedule("Samedi", SaturdayAmOpen, SaturdayAmClose, SaturdayPmOpen, SaturdayPmClose, rp ));
		schedule.add(scSunday.NewInstanceSchedule("Dimanche", SundayAmOpen, SundayAmClose, SundayPmOpen, SundayPmClose, rp ));
				
		rp.setListSchedule(schedule);	
		
		if (checkEmptyFields(rp)&&testPhoneOk(rp.getPhone())) {
		
		this.relayPointBusiness.insertRelayPoint(rp);
		
		modelView.addObject("Register", rp);
		modelView.setViewName("/relayPointRegister");
		
				return "/relayPointManage";
			}
		else {modelView.setViewName("/relayPointRegister");		
			
				return "home";
			}

			
		}
	
	/**Modification d'un point-Relais existant
	 * 
	 * Récupère les informations du formulaire "PreFilledFormUpdateRelayPoint", les vérifie et les enregistre dans la base de données en cas de succès. 
	 * 
	 * @Author : Ju
	 * @param name Le nom du point relais
	 * @param streetNumber Le numéro de la rue
	 * @param streetName Le nom de la rue
	 * @param complement Complément d'adresse éventuel
	 * @param city La ville
	 * @param postalCode Le code postal
	 * @param country Le pays
	 * @param phone Le numéro de téléphone
	 * @param MondayAmOpen Heure d'ouverture du lundi matin
	 * @param MondayAmClose Heure de fermeture du lundi matin
	 * @param MondayPmOpen Heure d'ouverture du lundi après-midi
	 * @param MondayPmClose Heure de fermeture du lundi après-midi
	 * @param TuesdayAmOpen Heure d'ouverture du mardi matin
	 * @param TuesdayAmClose Heure de fermeture du mardi matin
	 * @param TuesdayPmOpen Heure d'ouverture du mardi après-midi
	 * @param TuesdayPmClose Heure de fermeture du mardi après-midi
	 * @param WednesdayAmOpen Heure d'ouverture du mercredi matin
	 * @param WednesdayAmClose Heure de fermeture du mercredi matin
	 * @param WednesdayPmOpen Heure d'ouverture du mercredi après-midi
	 * @param WednesdayPmClose Heure de fermeture du mercredi après-midi
	 * @param ThursdayAmOpen Heure d'ouverture du jeudi matin
	 * @param ThursdayAmClose Heure de fermeture du jeudi matin
	 * @param ThursdayPmOpen Heure d'ouverture du jeudi après-midi
	 * @param ThursdayPmClose Heure de fermeture du jeudi après-midi
	 * @param FridayAmOpen Heure d'ouverture du vendredi matin
	 * @param FridayAmClose Heure de fermeture du vendredi matin
	 * @param FridayPmOpen Heure d'ouverture du vendredi après-midi
	 * @param FridayPmClose Heure de fermeture du vendredi après-midi
	 * @param SaturdayAmOpen Heure d'ouverture du samedi matin
	 * @param SaturdayAmClose Heure de fermeture du samedi matin
	 * @param SaturdayPmOpen Heure d'ouverture du samedi après-midi
	 * @param SaturdayPmClose Heure de fermeture du samedi après-midi
	 * @param SundayAmOpen Heure d'ouverture du dimanche matin
	 * @param SundayAmClose Heure de fermeture du dimanche matin
	 * @param SundayPmOpen Heure d'ouverture du dimanche après-midi
	 * @param SundayPmClose Heure de fermeture du dimanche après-midi
	 * @param modelView Le modèle qui comportera les objets et url
	 * @return String : page à afficher
	 */
	
	
	@RequestMapping(value = "/editRelayPoint", method = RequestMethod.POST)
	public String editRelayPoint(
		
			@RequestParam(value = "id") Long id,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "streetNumber") String streetNumber, 
			@RequestParam(value = "streetName") String streetName,
			@RequestParam(value = "complement") String complement, 
			@RequestParam(value = "city") String city,
			@RequestParam(value = "postalCode") String postalCode,
			@RequestParam(value = "country") String country, 
			@RequestParam(value = "phone") String phone,
			
			@RequestParam(value = "MondayAmOpen") String MondayAmOpen,
			@RequestParam(value = "MondayAmClose") String MondayAmClose,
			@RequestParam(value = "MondayPmOpen") String MondayPmOpen,
			@RequestParam(value = "MondayPmClose") String MondayPmClose,
			
			@RequestParam(value = "TuesdayAmOpen") String TuesdayAmOpen,
			@RequestParam(value = "TuesdayAmClose") String TuesdayAmClose,
			@RequestParam(value = "TuesdayPmOpen") String TuesdayPmOpen,
			@RequestParam(value = "TuesdayPmClose") String TuesdayPmClose,
			
			@RequestParam(value = "WednesdayAmOpen") String WednesdayAmOpen,
			@RequestParam(value = "WednesdayAmClose") String WednesdayAmClose,
			@RequestParam(value = "WednesdayPmOpen") String WednesdayPmOpen,
			@RequestParam(value = "WednesdayPmClose") String WednesdayPmClose,
			
			@RequestParam(value = "ThursdayAmOpen") String ThursdayAmOpen,
			@RequestParam(value = "ThursdayAmClose") String ThursdayAmClose,
			@RequestParam(value = "ThursdayPmOpen") String ThursdayPmOpen,
			@RequestParam(value = "ThursdayPmClose") String ThursdayPmClose,
			
			@RequestParam(value = "FridayAmOpen") String FridayAmOpen,
			@RequestParam(value = "FridayAmClose") String FridayAmClose,
			@RequestParam(value = "FridayPmOpen") String FridayPmOpen,
			@RequestParam(value = "FridayPmClose") String FridayPmClose,
			
			@RequestParam(value = "SaturdayAmOpen") String SaturdayAmOpen,
			@RequestParam(value = "SaturdayAmClose") String SaturdayAmClose,
			@RequestParam(value = "SaturdayPmOpen") String SaturdayPmOpen,
			@RequestParam(value = "SaturdayPmClose") String SaturdayPmClose,
			
			@RequestParam(value = "SundayAmOpen") String SundayAmOpen,
			@RequestParam(value = "SundayAmClose") String SundayAmClose,
			@RequestParam(value = "SundayPmOpen") String SundayPmOpen,
			@RequestParam(value = "SundayPmClose") String SundayPmClose,
			ModelAndView modelView) {	
		
		
		ScheduleBusiness scMonday = new ScheduleBusiness();	
		ScheduleBusiness scTuesday = new ScheduleBusiness();
		ScheduleBusiness scWednesday = new ScheduleBusiness();
		ScheduleBusiness scThursday = new ScheduleBusiness();
		ScheduleBusiness scFriday = new ScheduleBusiness();
		ScheduleBusiness scSaturday = new ScheduleBusiness();			
		ScheduleBusiness scSunday = new ScheduleBusiness();	
		
		RelayPoint rp= relayPointBusiness.retrieveRelayPointById(id).get();	
		rp.setId(id);
		rp.setName(name);
		rp.setStreetNumber(streetNumber);
		rp.setStreetName(streetName);
		rp.setComplement(complement);
		rp.setCity(city);
		rp.setPostalCode(postalCode);
		rp.setCountry(country);
		rp.setPhone(phone);		
		
		
		ArrayList<Schedule> schedule= new ArrayList <Schedule>();
		schedule.add(scMonday.NewInstanceSchedule("Lundi", MondayAmOpen, MondayAmClose, MondayPmOpen, MondayPmClose, rp ));
		schedule.add(scTuesday.NewInstanceSchedule("Mardi", TuesdayAmOpen, TuesdayAmClose, TuesdayPmOpen, TuesdayPmClose, rp ));
		schedule.add(scWednesday.NewInstanceSchedule("Mercredi", WednesdayAmOpen, WednesdayAmClose, WednesdayPmOpen, WednesdayPmClose, rp ));
		schedule.add(scThursday.NewInstanceSchedule("Jeudi", ThursdayAmOpen, ThursdayAmClose, ThursdayPmOpen, ThursdayPmClose, rp ));
		schedule.add(scFriday.NewInstanceSchedule("Vendredi", FridayAmOpen, FridayAmClose, FridayPmOpen, FridayPmClose, rp ));
		schedule.add(scSaturday.NewInstanceSchedule("Samedi", SaturdayAmOpen, SaturdayAmClose, SaturdayPmOpen, SaturdayPmClose, rp ));
		schedule.add(scSunday.NewInstanceSchedule("Dimanche", SundayAmOpen, SundayAmClose, SundayPmOpen, SundayPmClose, rp ));
				
		rp.setListSchedule(schedule);	
		
		if (checkEmptyFields(rp)&&testPhoneOk(rp.getPhone())) {
		
		relayPointBusiness.editRelayPoint(rp);
		
		modelView.addObject("listRelayPoint", relayPointBusiness.retrieveListRelayPoint(listRelayPoint));
		modelView.setViewName("/formEditRelayPoint");
		
				return "/relayPointManage";
			}
		else {modelView.setViewName("/relayPointRegister");		
			
				return "home";
			}

			
		}
		
	
	/** Lister les points relais
	 * 
	 * Récupère tous les points en relais enregistrés dans la bdd
	 * 
	 * @author: ju
	 * @param mv Le modèle qui comportera les objets et url
	 * @return ModelAndView
	 */
	@GetMapping(value = {"/ListRelayPoint" })
	public ModelAndView home(ModelAndView mv) {	
		mv.addObject("listRelayPoint", relayPointBusiness.retrieveListRelayPoint(listRelayPoint));
		mv.setViewName("formEditRelayPoint");
		return mv;
	}
	
	/** Trier par id
	 * 
	 * Récupère tous les points en relais enregistrés dans la bdd, et les organise par id
	 * 
	 * @author: ju
	 * @param mv Le modèle qui comportera les objets et url
	 * @return ModelAndView
	 */
	@GetMapping(value = { "/sortById" })
	public ModelAndView SortById(ModelAndView mv) {	
		mv.addObject("listRelayPoint", relayPointBusiness.retrieveListRelayPointSortedById(listRelayPoint));
		mv.setViewName("formEditRelayPoint");
		return mv;
	}
	
	/** Trier par nom
	 * 
	 * Récupère tous les points relais enregistrés dans la bdd, et les organise par nom
	 * 
	 * @author: ju
	 * @param mv Le modèle qui comportera les objets et url
	 * @return ModelAndView
	 */
	@GetMapping(value = { "/sortByName" })
	public ModelAndView SortByName(ModelAndView mv) {	
		mv.addObject("listRelayPoint", relayPointBusiness.retrieveListRelayPointSortedByName(listRelayPoint));
		mv.setViewName("formEditRelayPoint");
		return mv;
	}
	
	/** Trier par ville
	 * 
	 * Récupère tous les points relais enregistrés dans la bdd, et les organise par ville
	 * 
	 * @author: ju
	 * @param mv Le modèle qui comportera les objets et url
	 * @return ModelAndView
	 */
	@GetMapping(value = { "/sortByCity" })
	public ModelAndView SortByCity(ModelAndView mv) {	
		mv.addObject("listRelayPoint", relayPointBusiness.retrieveListRelayPointSortedByCity(listRelayPoint));
		mv.setViewName("formEditRelayPoint");
		return mv;
	}
	
	
	/** Modification d'un point Relais
	 * 
	 * Récupère l'id du point Relais à modifier en paramètre, afin de le modifier dans la bdd
	 * 
	 * @author: ju
	 * @param id l'id du point Relais à supprimer
	 * @param mv Le modèle qui comportera les objets et url
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/updateRelayPoint/{id}", method = RequestMethod.GET)	
	public ModelAndView UpdateRelayPoint(
			@PathVariable("id")Long id,
			ModelAndView mv) {	
		
		List<Schedule> LS = relayPointBusiness.retrieveRelayPointById(id).get().getListSchedule();		
	
		mv.addObject("UpdateRelayPoint", relayPointBusiness.retrieveRelayPointById(id).get());	
		mv.addObject("UpdateRelayPointSchedule", relayPointBusiness.retrieveRelayPointById(id).get().getListSchedule());	
		mv.setViewName("/PreFilledFormUpdateRelayPoint");
		return mv;
	}
	
	/**Suppression d'un point relais
	 * 
	 * Récupère l'id du point Relais à supprimer en paramètre, afin de le supprimer de la bdd
	 * 
	 * 
	 * @author: ju
	 * @param id l'id du point Relais à supprimer
	 * @param mv Le modèle qui comportera les objets et url
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/deleteRelayPoint/{id}", method = RequestMethod.GET)	
	public ModelAndView DeleteRelayPoint(
			@PathVariable("id")Long id,
			ModelAndView mv) {	
		
		relayPointBusiness.deleteRelayPoint(id);
		
		mv.addObject("listRelayPoint", relayPointBusiness.retrieveListRelayPoint(listRelayPoint));
		mv.setViewName("formEditRelayPoint");
		return mv;
	}
	
	/**
	 * Méthode vérifiant si les champs obligatoires du point Relais sont vides. Si oui, la méthode retourne false
	 * 
	 * @Author : Ju
	 * @param Objet RelayPoint
	 * @return Boolean : les champs sont-ils vides ? Si non ? false : true
	 */
	public boolean checkEmptyFields(RelayPoint rp) {
		boolean isOneFieldEmpty = false;
		
		if (rp.getName()!=null && 
			rp.getStreetNumber() !=null &&
			rp.getStreetName() !=null &&	
			rp.getCity()!=null &&
			rp.getPostalCode() !=null &&
			rp.getCountry() !=null &&
			rp.getPhone()!=null){
			
		isOneFieldEmpty = true;
			
	}	        
		
	return isOneFieldEmpty;
	}
	
	/**
	 * Méthode vérifiant si le format du numéro de téléphone entré respecte le format international
	 * 
	 * @Author : Ju
	 * @param String content le numéro de téléphone
	 * @return Boolean : le numéro respecte-t-il le format international ? Si non ? false : true
	 */
	public boolean testPhoneOk(String phone) {
		boolean isPhoneOk = false;
		String regexMatcher = "^\\+(([\\d]{1,3})||([1-][\\d]{3}))[\\d]{7,9}$";
		if(phone.matches(regexMatcher)) {
			isPhoneOk = true;
		}
		return isPhoneOk;
	
}
}
