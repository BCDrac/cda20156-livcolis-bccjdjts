<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Gestion des points relais</title>
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
	
	<body>
	
	<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des points relais.</p>		<button onclick="self.location.href='disconnect'">Se déconnecter</button>
	
		<div>
		
 			<figure>
 				<a href="${pageContext.request.contextPath}/relayPointRegister">
					<img src="${pageContext.request.contextPath}/resources/img/relayBuild.png"
		         	alt="Création de point relais">
	        	</a>
	  			<figcaption>Créer un point de relais</figcaption>
 			</figure>
		
 			<figure>
 				<a href="${pageContext.request.contextPath}/ListRelayPoint">
					<img src="${pageContext.request.contextPath}/resources/img/relayEdit.png"
		         	alt="Modification de point relais">
	        	</a>
	  			<figcaption>Modifier un point relais</figcaption>
 			</figure>

		</div>
		
		<br/>
		<button onclick="self.location.href='delivererDashboard'">Retour</button>
	
	</body>
</html>