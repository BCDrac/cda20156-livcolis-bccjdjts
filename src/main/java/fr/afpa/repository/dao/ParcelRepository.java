package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.dao.Parcel;

public interface ParcelRepository extends JpaRepository<Parcel, Long> {
	
	
	/**
	 * @author Djé
	 * Méthode définie car le CRUD ne porte que sur l'id
	 * 
	 * @param code : String
	 * @return objet de type Parcel
	 */
	public Parcel findByCode(String code);

}