******* cda20156-livcolis-bccjdjts *******

Colis-Trot'Vous souhaite la bienvenue et vous remercie d'avoir choisit ses services.
Ce ReadMe vous guidera sur les fa�ons d'utiliser le site web.



======= Pr�-requis =======

Avant de commencer, il vous faut effectuer les �tapes suiavntes :



= 1 === La base de donn�es ===

Le site repose sur l'utilisation d'une base de donn�es et le syst�me de gestion de base de donn�es PostGreSQL (version 10 ou plus).
Il vous faudra cr�er la base de donn�es et le profil � l'aide de PGAdmin 4.


*** Le profil ***

Dans PGAdmin, faites un clic droit sur PostgreSQL (num�ro de version) et faites Create > Login/Group role.
Entrez le nom test (dans General) et le mot de passe test (dans Definition), accordez tous les privileges � ce profil.


*** La base de donn�es ***

Dans PGAdmin, faites un clic droit sur Database et faites Create > Database.
Il vous faudra cr�er deux bases de donn�es : livcolis et livcolis-test.
La premi�re servira au fonctionnement du site, la seconde est r�serv�e aux tests unitaires.

Une fois les bases cr��es, utilisez un logiciel d'administration de base de donn�es comme DBeaver.
Connectez-vous en faisant New database connection (symbole d'une prise �lectrique bleue marqu�e d'un "plus" vert, en haut � gauche dans le menu de t�te).
Choisissez PostgreSQL. Dans "database", entrez livcolis, dans "Username", entrez test et pour "password" entrez test. Cliquez sur finish pour terminer. Faites de m�me pour la base livcolis-test.

L� deux choix s'offrent � vous pour cr�er les tables : vous pouvez laisser le site se charger de les cr�er ou utiliser le script SQL fourni. Pour utiliser le script, dans DBeaver, s�lectionner la base de donn�es concern�e et appuyez sur F3, choisissez "new script" puis glissez-d�posez le script sur la zone pour ouvrir le fichier. Choisissez "Execute SQL Script" sur le c�t� de la fen�tre (3�me ic�ne en partant du haut) et laissez les tables se cr�er.
Faites de m�me pour livcolis-test.

Une fois les tables cr��es, il vous faudra encore quelques �l�ments.


= 2 === Java et Tomcat ===

Le site utilise le langage Java pour tourner, il vous faudra donc avoir une version r�cente (1.8) ainsi que la JDK, disponibles sur le site de Oracle. Il vous faudra �galement utiliser le serveur TomCat (version 9).Lancez le serveur (une fois qu'il est install�, vous pouvez allez dans son dossier et choisir bin > startup.bat)
En ouvrant un navigateur web et en entrant http://localhost:8080/ dans la barre d'adresse, vous pourrez voir le panneau de gestion de TomCat.
Cliquez sur Manager App, allez dans "Fichier WAR � d�ployer" et s�lectionner le fichier War join au dossier de livrables. Apr�s avoir cliqu� sur "/cda20156-livcolis-bccjdjts" dans le tableau "Applications" pour lancer le site.


= 3 === Le site Colis-Trot' ===


*** En tant qu'utilisateur ***

Si vous attendez un colis pris en charge par Colis-Trot', vous avez d� recevoir un mail contenant un code permettant de suivre le colis. Entrez le dans la zone de recherche. Si le colis existe bien, vous verrez un sch�ma pr�sentant le stade d'avancement de la livraison. � chaque �tape de livraison, vous recevrez un message dans votre bo�te email vous informant du stade de livraison.


*** En tant que livreur ***

Vous pouvez vous inscrire via le lien vers le formulaire sur la page d'acceuil. Entrez les informations demand�es. Le num�ro de t�l�phone doit comporter un + et 10 chiffres (12 maximum).
Le Siret est compos� de 14 chiffres et choisissez un login et un mot de passe qui vous serviront � vous connecter. Une fois enregistr�, vous serez redirig� vers le formulaire de login pour vous identifier.

Sur le tableau de bord, 4 options s'offrent � vous : g�rer les colis, les points relais, les utilisateur et voir votre compte.
Avant de cr�er un colis, il vous faudra des utilisateurs et points-relais.

Lors de la cr�ation d'un point-relais, il vous sera demand� d'attribuer un nom au point-relais, son adresse et un num�ro de t�l�phone. Un tableau d'horaires vous sera propos�, vous pouvez modifier les chiffres en accordance avec les horaires du point-relais.
Vous aurez toujours la possibilit� de modifier le point-relais sur le menu de gestion. Il vous faudra au moins 3 points-relais avant de pouvoir envoyer un colis.


Il vous faut aussi des utilisateurs, au moins deux, un �meteur et un recepteur. Le formulaire d'ajout de client vous demandera l'adresse mail de chaque utilisateur. Faites attention � bien entrer les adresses, car le destinataire d'un colis recevra des informations sur sa livraison via le syst�me d'email depuis le compte afpa-cda-test@hotmail.com


Une fois vos points-relais et utilisateurs cr��s, vous pouvez cr�er un colis.
Le formulaire vous demandera l'adresse mail d'un exp�diteur et destinataire (ces derniers doivent exister dans la base de donn�es) ainsi que les noms des points-relais par lesquels passera le colis (ces points-relais doivent �galement exister dans la base de donn�es).
Une fois le colis enregistr�, le client recevra un mail contenant le code lui permettant de suivre son colis via la recherche du site.

Pour mettre � jour le colis, il vous faudra le modifier et cocher la case suivant son stade de voyage (interm�diaire ou arriv�e). Le client recevra un mail lors de chaque changement.


En tant que livreur, vous pouvez voir votre profil et modifier vos informations. Notez cependant que vous ne pouvez modifier votre login ou adresse mail.