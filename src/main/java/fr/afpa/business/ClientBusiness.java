package fr.afpa.business;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.dao.Client;
import fr.afpa.repository.dao.ClientRepository;

@Service
public class ClientBusiness {
	
	@Autowired
	private ClientRepository clientRepository;
	
	
	/**
	 * @author Djé
	 * Méthode qui permet d'instancier un client puis de le rentrer en base de données dans la table Client. L'attribut clientRepository permet d'avoir toutes les méthodes héritées
	 * de JpaRepository
	 * 
	 * @param lastName Nom de famille saisi
	 * @param firstName Prénom saisi
	 * @param email Email saisi
	 * @param phone Numéro de téléphone saisi
	 */
	public void createClient(String lastName, String firstName, String email, String phone) {
		Client createClient = new Client(lastName, firstName, email, phone);
		clientRepository.save(createClient);
	}
	
	
	/**
	 * @author Djé
	 * Méthode qui permet de récupérer tous les clients à destinations de page jsp listClients
	 * 
	 * @return listClients  liste des clients en base de données
	 */
	public ArrayList<Client> retrieveClients(){
		ArrayList<Client> listClients = (ArrayList<Client>) clientRepository.findAll();
		return listClients;
	}
	
	
	/**
	 * @author Djé
	 * Méthode qui permet de récupérer un client via son id récupéré en variable de chemin via la view listClients
	 * 
	 * 
	 * @param identificator envoie en paramètre l'identifiant qui a transité depuis la view jusqu'au model ici via la variable de chemin
	 * @return clientById : Optional de type Client
	 */
	public Optional<Client> retrieveClientFromId(Long identificator){
		Optional<Client> clientById = clientRepository.findById(identificator);
		return clientById;
	}
	
	
	/**
	 * @author Djé
	 * Méthode qui permet de modifier un client. Il faut passer tous les paramètres de la saisie pour l'instance et y compris l'id, même s'il s'agit d'une séquence
	 * car il faut aller pointer sur la primary key et flush la ligne correspondante avec les nouvelles datas
	 * 
	 * @param id id client à modifier
	 * @param lastName Nom de famille saisi
	 * @param firstName Prénom saisi
	 * @param email Email saisi
	 * @param phone Numéro de téléphone saisi
	 */
	public void editClient(Long id, String lastName, String firstName, String email, String phone) {
		Client editClient = new Client(lastName, firstName, email, phone);
		editClient.setId(id);
		clientRepository.saveAndFlush(editClient);
	}
	
	
	/**
	 * @author Djé
	 * Cette méthode vient supprimer le client qui correspond à l'id passé en paramètre
	 * 
	 * @param id du client
	 */
	public void deleteClient(Long id) {
		clientRepository.deleteById(id);
	}

	/**
	 * @author sofiane Tayeb
	 * Méthode de recherche des client par email
	 * 
	 * @param mail email du client
	 */
	public Client findClientByMail(String mail) {
		return clientRepository.findByEmail(mail);
		
	}
	
	
	
}
