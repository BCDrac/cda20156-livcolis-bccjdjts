package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.dao.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

	Client findByEmail(String mail);

}