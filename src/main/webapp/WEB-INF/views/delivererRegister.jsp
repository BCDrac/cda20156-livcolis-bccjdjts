<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>S'inscrire</title>
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
	
	<body>

	<form action="delivererRegister" method="post">
	
		<c:out value="${errorList['lastName']}"/><br/>
		<label>Nom : </label> <input type="text" name="lastName" value="${deliverer.lastName}" required><br/>
		
		<c:out value="${errorList['firstName']}"/><br/>
		<label>Prénom : </label> <input type="text" name="firstName" value="${deliverer.firstName}" required><br/>
		
		<c:out value="${errorList['email']}"/><br/>
		<label>Email : </label> <input type="email" name="email" value="${deliverer.email}" required><br/>
		
		<c:out value="${errorList['phone']}"/><br/>
		<label>Téléphone : </label> <input type="tel" name="phone" maxlength="13" pattern="^\+(([\d]{1,3})|([1-][\d]{3}))[\d]{7,9}$" value="${deliverer.phone}" required><br/>
		
		<c:out value="${errorList['siret']}"/><br/>
		<label>Siret : </label> <input type="text" name="siret" maxlength="14" pattern="^[0-9]{14}$" value="${deliverer.siret}" required><br/>
		
		<br/>

		<c:out value="${errorList['username']}"/><br/>
		<label>Login : </label> <input type="text" name="username" value="${login.username}" required><br/>
		
		<c:out value="${errorList['password']}"/><br/>
		<label>Mot de passe : </label> <input type="password" name="password" required><br/>
		
		<c:out value="${errorList['passwordCheck']}"/><br/>
		<label>Réécrivez votre mot de passe : </label> <input type="password" name="passwordCheck" required><br/>
		<br/>		
		<button type="submit" >Envoyer</button>
		
	</form>
	<br/>
	<button onclick="self.location.href='home'">Retour</button>

	</body>
</html>