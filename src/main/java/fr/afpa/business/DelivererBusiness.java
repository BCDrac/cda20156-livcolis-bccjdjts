package fr.afpa.business;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.dao.Deliverer;
import fr.afpa.repository.dao.DelivererRepository;

/**
 * Couche métier s'occupant du livreur
 * 
 * @author Cécile
 */
@Service
public class DelivererBusiness {
	
	@Autowired
	private DelivererRepository delivererRepository;
	
	
	/**
	 * Insère (save) un Deliverer (livreur) en base de données.
	 * 
	 * @param deliverer l'objet Deliverer à insérer.
	 */
	public void insertDeliverer(Deliverer deliverer) {
		this.delivererRepository.save(deliverer);
	}
	
	
	/**
	 * Vérifie si un email existe déjà en base de données
	 * 
	 * @param email l'email à rechercher.
	 * @return boolean : retourne true si l'email existe.
	 */
	public boolean isEmailUsed(String email) {
		return this.delivererRepository.findByEmail(email) != null;
	}
	
	
	/**
	 * Sélectionne un livreur d'après son id
	 * 
	 * @param id l'id correspondant au livreur à trouver
	 * @return Optional Deliverer : retourne un objet optionnal de Deliverer
	 */
	public Optional<Deliverer> findById(Long id) {
		return this.delivererRepository.findById(id);
	}
	
	
	/**
	 * Met à jour les informations d'un livreur
	 * 
	 * @param deliverer le livreur à mettre à jour
	 */
	public void updateDeliverer(Deliverer deliverer) {
		this.delivererRepository.saveAndFlush(deliverer);
	}
	
	
	/**
	 * Supprime un livreur
	 * 
	 * @param deliverer le livreur à supprimer
	 * @author Cécile
	 */
	public void deleteDeliverer(Deliverer deliverer) {
		this.delivererRepository.delete(deliverer);
	}


	/**
	 * Récupère un livreur par son email
	 * 
	 * @param email l'emaal servant à récupérer le livreur
	 * @return Deliverer : retourne un livreur trouvé grâce à l'email fourni
	 */
	public Deliverer findByEmail(String email) {
		return this.delivererRepository.findByEmail(email);
	}


}