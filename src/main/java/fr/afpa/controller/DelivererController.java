package fr.afpa.controller;

import java.util.HashMap;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.dao.Deliverer;
import fr.afpa.beans.dao.Login;
import fr.afpa.business.DelivererBusiness;
import fr.afpa.business.LoginBusiness;


/**
 * Contrôleur du livreur (Deliverer)
 * 
 * @author Cécile
 */
@SessionScope
@Controller
@SessionAttributes("id")
public class DelivererController {

	@Autowired
	private DelivererBusiness delivererBusiness;
	
	@Autowired
	private LoginBusiness loginBusiness;

	private HashMap<String, String> errorList;

	/**
	 * Déconnecte l'utilisateur : détruit sa session et le redirige à l'accueil du site
	 * @param session session
	 * @return String : la page vers laquelle rediriger
	 * @author Cécile
	 */
	@RequestMapping(value = "/disconnect", method = RequestMethod.GET)
	public String disconnect(HttpSession session) {
		session.invalidate();
		return "home";
	}
	
	
	
	/**
	 * Redirige vers le formulaire d'inscription livreur
	 * 
	 * @return String : l'url vers laquelle se rediriger
	 * 
	 * @author Cécile
	 */
	@RequestMapping(value = "/delivererRegister", method = RequestMethod.GET)
	public String redirectToRegisterForm() {
		return "delivererRegister";
	}

	
	/**
	 * Inscription du livreur.
	 * 
	 * Récupère les informations du formulaire d'inscription livreur, les vérifie et enregistre dans la base de données en cas de succès.
	 * 
	 * @param lastName le nom de famille du livreur
	 * @param firstName le prénom du livreur
	 * @param email l'email du livreur
	 * @param phone le numéro de téléphone du livreur
	 * @param username l'identifiant du livreur lui servant à se connecter
	 * @param password le mot de passe du livreur lui servant à se connecter
	 * @param passwordCheck permet de s'assurer que le livreur est sûr de son mot de passe
	 * @param siret le numéro siret de l'entreprise pour laquelle travaille le livreur
	 * @param modelView sert à insérer des objets et rédiriger vers des pages jsp
	 * @return ModelAndView : le modèle qui comportera les objets et url
	 * 
	 * @author Cécile
	 */
	@RequestMapping(value = "/delivererRegister", method = RequestMethod.POST)
	public ModelAndView insertDeliverer(
			@RequestParam(value = "lastName")String lastName,
			@RequestParam(value = "firstName") String firstName,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "phone") String phone,
			@RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "passwordCheck") String passwordCheck,
			@RequestParam(value = "siret") String siret,
			ModelAndView modelView) {
		
		this.errorList = new HashMap<String, String>();
		
		this.checkUser(lastName.trim(), firstName.trim(),phone.trim(), siret.trim());
		this.checkEmail(email);
		this.checkAccount(username.trim(), password, passwordCheck);
		
		Deliverer deliverer = new Deliverer();

		deliverer.setLastName(lastName.trim());
		deliverer.setFirstName(firstName.trim());
		deliverer.setEmail(email.trim());
		deliverer.setPhone(phone.trim());
		deliverer.setSiret(siret.trim());

		Login login = new Login();
		login.setUsername(username.trim());
		login.setPassword(password);
		
		if (this.errorList.isEmpty()) {
			
			login.setDeliverer(deliverer);

			this.loginBusiness.insert(login);
			
			modelView.addObject("registerSuccess", "Compte créé, vous pouvez vous identifier.");
			modelView.setViewName("delivererAuthentication");
		} else {
			modelView.addObject("deliverer", deliverer);
			modelView.addObject("login", login);
			modelView.addObject("errorList", this.errorList);
			modelView.setViewName("delivererRegister");
		}
		return modelView;
	}
	
	
	@RequestMapping(value = "/delivererDashboard", method = RequestMethod.GET)
	public ModelAndView redirectToDashboard(ModelAndView modelView) {
		modelView.setViewName("delivererDashboard");
		return modelView;
	}
	
	
	/**
	 * Redirige vers la page de profil du livreur
	 * 
	 * @param modelView sert à insérer des objets et rédiriger vers des pages jsp
	 * @return ModelAndView : le modèle qui comportera les objets et url
	 * 
	 * @author Cécile
	 */
	@RequestMapping(value = "/delivererAccount", method = RequestMethod.GET)
	public ModelAndView redirectToDelivererAccount(ModelAndView modelView) {
		modelView.setViewName("delivererProfile");
		return modelView;
	}
	
	
	/**
	 * Redirige vers la modification de profil du livreur
	 * 
	 * @param modelView sert à insérer des objets et rédiriger vers des pages jsp
	 * @return ModelAndView : le modèle qui comportera les objets et url
	 * 
	 * @author Cécile
	 */
	@RequestMapping(value = "/editAccount", method = RequestMethod.GET)
	public ModelAndView updateDelivererAccount(ModelAndView modelView) {

		// Optional<Deliverer> deliverer = this.delivererBusiness.findById((long) 20);
		modelView.setViewName("delivererDashboard");
		modelView.addObject("update", true);
		modelView.setViewName("delivererProfile");

		return modelView;
	}
	
	
	/**
	 * Permet de mettre à jour les informations du livreur
	 * 
	 * @param id l'id du livreur à mettre à jour
	 * @param lastName le nom à mettre à jour
	 * @param firstName le prénom à mettre à jour
	 * @param phone le téléphone à mettre à jour
	 * @param username le pseudo à mettre à jour
	 * @param password le mot de passe à mettre à jour
	 * @param passwordCheck vérification du mot de passe choisi
	 * @param siret le siret à mettre à jour
	 * @param modelView sert à insérer des objets et rédiriger vers des pages jsp
	 * @return ModelAndView : le modèle qui comportera les objets et url
	 */
	@RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
	public ModelAndView updateDelivererAccount(
			@RequestParam(value = "id")Long id,
			@RequestParam(value = "lastName")String lastName,
			@RequestParam(value = "firstName") String firstName,
			@RequestParam(value = "phone") String phone,
			@RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "passwordCheck") String passwordCheck,
			@RequestParam(value = "siret") String siret,
			ModelAndView modelView) {
		
		this.errorList = new HashMap<String, String>();

			this.checkUser(lastName.trim(), firstName.trim(), phone.trim(), siret.trim());
			
			if (password != null && !password.isEmpty()) {
				this.checkPassword(password, passwordCheck);
			}
			
			Deliverer deliverer = this.delivererBusiness.findById(id).get();
			
			if (this.errorList.isEmpty()) {
				
				deliverer.setLastName(lastName.trim());
				deliverer.setFirstName(firstName.trim());
				deliverer.setPhone(phone.trim());
				deliverer.setSiret(siret.trim());
				
				if (password != null && !password.isEmpty()) {
					deliverer.getLogin().setPassword(password.trim());
					deliverer.getLogin().setDeliverer(deliverer);
				}
				
				this.loginBusiness.update(deliverer.getLogin());
	
				modelView.addObject("deliverer", deliverer);
				modelView.setViewName("delivererProfile");
	
			} else {
				modelView.addObject("deliverer", deliverer);
				modelView.addObject("errorList", this.errorList);
				modelView.addObject("update", true);
				modelView.setViewName("delivererProfile");
			}
		return modelView;
	}


	/**
	 * Vérifie la validité des informations du livreur.
	 * 
	 * Combine les méthodes checkNames(String, String), checkEmail(String), checkPhone(String) et checkSiret(String).
	 * 
	 * @param lastName le nom de famille du livreur.
	 * @param firstName le prénom du livreur.
	 * @param phone le numéro de téléphone du livreur.
	 * @param siret le numéro siret de la compagnie du livreur.
	 * 
	 * @author Cécile
	 */
	private void checkUser(String lastName, String firstName, String phone, String siret) {
		this.checkNames(lastName, firstName);
		this.checkPhone(phone);
		this.checkSiret(siret);
	}
	
	
	/**
	 * Vérifie que les informations de création d'identifiants sont valides.
	 * 
	 * Combine les méthodes checkUsername(String) et checkPassword(String).
	 * 
	 * @param username le login à vérifier.
	 * @param password le mot de passe servant à l'identification.
	 * @param passwordCheck le mot de passe de vérification.
	 * 
	 * @author Cécile
	 */
	private void checkAccount(String username, String password, String passwordCheck) {
		this.checkUsername(username);
		this.checkPassword(password, passwordCheck);
	}
	
	
	/**
	 * Vérifie l'existence des noms du livreur.
	 * 
	 * @param lastName le nom de famille du livreur.
	 * @param firstName le prénom du livreur.
	 * 
	 * @author Cécile
	 */
	private void checkNames(String lastName, String firstName) {
		if (lastName == null || lastName.isEmpty()) {
			this.errorList.put("lastName", "Erreur : veuillez entrer un nom.");
		}
		
		if (firstName == null || firstName.isEmpty()) {
			this.errorList.put("firstName", "Erreur : veuillez entrer un prénom.");
		}
	}
	
	
	/**
	 * Vérifie si un email n'est pas déjà présent en base de données.
	 * 
	 * @param email l'email à vérifier.
	 * 
	 * @author Cécile
	 */
	private void isEmailUsed(String email) {
		if (this.delivererBusiness.isEmailUsed(email)) {
			this.errorList.put("email", "Erreur : email non disponible. Veuillez entrer une autre adresse.");
		}
	}
	
	
	/**
	 * Vérifie qu'un email est valide
	 * 
	 * @param email l'email à vérifier
	 * 
	 * @author Cécile
	 */
	private void checkEmail(String email) {
		if (email != null && !email.isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("((^[a-z])[\\W\\w]{1,}[^\\W_]@[0-9a-z]+.[a-z]{2,3}$)");
	
			matcher = pattern.matcher(email);
			
			if (!matcher.matches()) {
				this.errorList.put("email", "Erreur : veuillez entrer un email au format valide.");
			} else { 
				this.isEmailUsed(email);
			}
		} else {
			this.errorList.put("email", "Erreur : veuillez entrer un email.");
		}
	}
	
	
	/**
	 * Vérifie qu'un numéro de téléphone est valide (composé de 10 chiffres).
	 * 
	 * @param phone le numéro de téléphone à vérifier.
	 * 
	 * @author Cécile
	 */
	private void checkPhone(String phone) {
		if (phone != null && !phone.isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^\\+(([\\d]{1,3})|([1-][\\d]{3}))[\\d]{7,9}$");

			matcher = pattern.matcher(phone);
			if (!matcher.matches()) {
				this.errorList.put("phone", "Erreur : le numéro de téléphone n'est pas au bon format.");
			}
		} else {
			this.errorList.put("phone", "Erreur : veuillez entrer un numéro de téléphone.");
		}
	}
	
	
	/**
	 * Vérifie qu'un numéro siret est valide (composé de 14 chiffres).
	 * 
	 * @param siret le numéro siret à vérifier.
	 * 
	 * @author Cécile
	 */
	private void checkSiret(String siret) {
		if (siret != null && !siret.isEmpty()) {
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile("^[0-9]{14}$");

			matcher = pattern.matcher(siret);

			if (!matcher.matches()) {
				this.errorList.put("siret", "Erreur : le SIRET n'est pas au bon format.");
			}
		} else {
			this.errorList.put("siret", "Erreur : veuillez entrer un SIRET.");
		}
	}
	
	
	/**
	 * Vérifie la présence d'un login.
	 * @param username le login à vérifier.
	 * 
	 * @author Cécile
	 */
	private void checkUsername(String username) {
		if (username == null || username.isEmpty()) {
			this.errorList.put("username", "Erreur, veuillez entrer un login.");
		} else if (this.loginBusiness.isUsernameUsed(username)) {
			this.errorList.put("username", "Erreur, veuillez choisir un autre login.");
		}
	}
	
	
	/**
	 * Vérifie qu'un mot de passe choisi est similaire au mot de passe de vérification.
	 * 
	 * @param password le mot de passe à comparer.
	 * @param passwordCheck le mot de passe de vérification.
	 * 
	 * @author Cécile
	 */
	private void checkPassword(String password, String passwordCheck) {
		if (password == null || password.isEmpty() || passwordCheck == null || passwordCheck.isEmpty()) {
			this.errorList.put("password", "Erreur : veuillez entrer un mot de passe.");
			
		} else if (!password.equals(passwordCheck)) {
			this.errorList.put("passwordCheck", "Erreur : les mots de passe ne correspondent pas.");
		}
	}
	
	
	/**
	 * méthode redirection à la page de connexion
	 * @author Sofiane Tayeb
	 *  
	 */
	@RequestMapping(value = "/delivererAuthentication", method = RequestMethod.GET)
	public String redirectToAuthenticationForm() {
		return "delivererAuthentication";
	}
	
	/**
	 * * @author Sofiane Tayeb
	 * la page de connexion pour un transporteur 
	 * si l'authentification est bonne le deliverer se redireger vers la page homePanel
	 * sinon l'utilisateur reste sur la page de connexion avec un message d'erreur
	 * @param login: le login de l'utilisateur
	 * @param password: le mot de passe de l'utilisateur
	 * @return model: une vue contenant : l'objet transporteur si les données de connexion sont bonnes, un message d'erreur sinon
	 */
	@RequestMapping(value = "/delivererCheckConection", method = RequestMethod.POST)
	public ModelAndView authenticationDeliverer(
			@RequestParam(value = "login")String login,
			@RequestParam(value = "password") String password,
			ModelAndView modelView) {
		Deliverer deliverer=loginBusiness.checkConection(login, password);
		if(deliverer!=null) {
			modelView.addObject("deliverer", deliverer);
			modelView.setViewName("delivererDashboard");
		}else {
			modelView.addObject("error", "votre login ou mot de passe est incorrect ");
			modelView.setViewName("delivererAuthentication");
		}
		return modelView;
	}
}