<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<header>
	<img src="${pageContext.request.contextPath}/resources/img/logo.png" id="logo"><h1 id="title">Colis-Trot'</h1>
</header>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/header.css"/>