<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Enregistrement d'un point relais</title>
</head>

	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>
<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des points relais.</p>
	<form action="insertRelayPoint" method="post">

		Nom : <input type="text" name="name" required><br /> 
		Numéro de rue : <input type="number" name="streetNumber" required><br>
		Nom de rue : <input	type="text" name="streetName" required><br> 
		Complément d'adresse :<input type="text" name="complement"><br> 
		Ville :	<input type="text" name="city" required><br> 
		Code Postal : <input type="text" name="postalCode" required><br> 
		Pays : <input type="text" name="country" required><br> 
		Téléphone : <input type="tel" name="phone" required><br>


		<TABLE BORDER="1">
			<CAPTION>Horaires</CAPTION>
			<TR>
				<TH>Jour</TH>
				<TH>Ouverture Matin</TH>
				<TH>Fermeture Matin</TH>
				<TH>Ouverture Après-Midi</TH>
				<TH>Fermeture Après-Midi</TH>
			</TR>

			<tr>
				<td>Lundi</td>
				<td><input type="text" name="MondayAmOpen" value='8h30' required></td>
				<td><input type="text" name="MondayAmClose" value='12h' required></td>
				<td><input type="text" name="MondayPmOpen" value='13h30' required></td>
				<td><input type="text" name="MondayPmClose" value='17h' required></td>
			</tr>
			<tr>
				<td>Mardi</td>
				<td><input type="text" name="TuesdayAmOpen" value='8h30' required></td>
				<td><input type="text" name="TuesdayAmClose" value='12h' required></td>
				<td><input type="text" name="TuesdayPmOpen" value='13h30' required></td>
				<td><input type="text" name="TuesdayPmClose" value='17h' required></td>
			</tr>
			<tr>
				<td>Mercredi</td>
				<td><input type="text" name="WednesdayAmOpen" value='8h30' required></td>
				<td><input type="text" name="WednesdayAmClose" value='12h' required></td>
				<td><input type="text" name="WednesdayPmOpen" value='13h30' required></td>
				<td><input type="text" name="WednesdayPmClose" value='17h' required></td>
			<tr>
				<td>Jeudi</td>
				<td><input type="text" name="ThursdayAmOpen" value='8h30' required></td>
				<td><input type="text" name="ThursdayAmClose" value='12h' required></td>
				<td><input type="text" name="ThursdayPmOpen" value='13h30' required></td>
				<td><input type="text" name="ThursdayPmClose" value='17h' required></td>
			<tr>
				<td>Vendredi</td>
				<td><input type="text" name="FridayAmOpen" value='8h30' required></td>
				<td><input type="text" name="FridayAmClose" value='12h' required></td>
				<td><input type="text" name="FridayPmOpen" value='13h30' required></td>
				<td><input type="text" name="FridayPmClose" value='17h' required></td>
			<tr>
				<td>Samedi</td>
				<td><input type="text" name="SaturdayAmOpen" value='8h30' required></td>
				<td><input type="text" name="SaturdayAmClose" value='12h' required></td>
				<td><input type="text" name="SaturdayPmOpen" value='fermé' required></td>
				<td><input type="text" name="SaturdayPmClose" value='fermé' required></td>
			</tr>
			<tr>
				<td>Dimanche</td>
				<td><input type="text" name="SundayAmOpen" value='fermé' required></td>
				<td><input type="text" name="SundayAmClose" value='fermé' required></td>
				<td><input type="text" name="SundayPmOpen" value='fermé' required></td>
				<td><input type="text" name="SundayPmClose" value='fermé' required></td>
			</tr>



		</TABLE>
		<button type="submit">Envoyer</button>
	</form>

	<button onclick="self.location.href='relayPointManage'">Retour</button>

</body>
</html>