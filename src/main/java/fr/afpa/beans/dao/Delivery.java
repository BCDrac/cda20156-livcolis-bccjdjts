package fr.afpa.beans.dao;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity

//SELECT delivery.idRelayPoint.idDeliverer.idParcel FROM Delivery delivery

public class Delivery {
//	@EmbeddedId
//    private IdDelivery idDelivery;	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="delivery_id_gen")
	@SequenceGenerator(name="delivery_id_gen", sequenceName="delivery_parcel", allocationSize=1)
	private Long id;
	@Column(nullable = false)
	private int step;
	@ColumnDefault("false")
	private boolean done;
	private LocalDate date;
	@Column(nullable = false)
	private String currentState;
	
	@OneToOne(cascade=CascadeType.ALL, mappedBy = "delivery")
    @JoinColumn(name = "id_mail", referencedColumnName = "id")
	private Mail mail;
	
	@ManyToOne()
    @JoinColumn(name = "id_deliverer", referencedColumnName = "id")
	private Deliverer deliverer;
	@ManyToOne()
    @JoinColumn(name = "id_relayPoint", referencedColumnName = "id")
	private RelayPoint relayPoint;
	
	@ManyToOne()
    @JoinColumn(name = "id_parcel", referencedColumnName = "id")
	private Parcel parcel;
	
}