<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>authentication</title>
    </head>
    
    <jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    
    <body>
      <form action="delivererCheckConection" method="post">
		<div class="container">
			<label for="uname"><b>Username</b></label> 
			<input type="text" placeholder="Enter Username" name="login" required> <br>
			<label for="psw"><b>Password</b></label> 
			<input type="password" placeholder="Enter Password" name="password" required><br>
			<label id="errorInput" style= "color:red";><c:out value="${ error }"></c:out></label><br>			
			<button type="submit">Login</button>
			
			
		</div>
	</form>
    </body>
</html>
