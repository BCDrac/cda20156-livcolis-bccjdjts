<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Rechercher client</title>
</head>
<body>
<header>
	<img src="${pageContext.request.contextPath}/resources/img/logo.png">
</header>
<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des clients.</p>
	<TABLE id="table" BORDER="1">
		<CAPTION>Liste des clients</CAPTION>
		<TR>
			<TH><a href="sortNum">Numéro client</a></TH>
			<TH><a href="sortName">Nom</a></TH>
			<TH><a href="sortFirstName">Prénom</a></TH>
			<TH><a href="sortEmail">Email</a></TH>
			<TH><a href="sortPhone">Téléphone</a></TH>
			<TH>Supprimer</TH>
			<TH>Modifier</TH>
		</TR>
		<c:forEach items="${ clientRetrieved }" var="client">
			<tr>
				<td><c:out value="${client.id}" /></td>
				<td><c:out value="${client.lastName}" /></td>
				<td><c:out value="${client.firstName}" /></td>
				<td><c:out value="${client.email}" /></td>
				<td><c:out value="${client.phone}" /></td>
				<td><a
					href="${pageContext.request.contextPath}/deleteClient/${client.id }">Supprimer</a></td>
				<td><a
					href="${pageContext.request.contextPath}/editClient/${client.id }">Modifier</a></td>
			</tr>
		</c:forEach>
	</TABLE>
	<a href="clientList">Retour</a>
</body>
</html>