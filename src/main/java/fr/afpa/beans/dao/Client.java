package fr.afpa.beans.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_id_gen")
	@SequenceGenerator(name = "client_id_gen", sequenceName = "sequ_client", allocationSize = 1)
	private Long id;
	@Column(nullable = false)
	private String lastName;
	@Column(nullable = false)
	private String firstName;
	@Column(nullable = false, unique = true)
	private String email;
	private String phone;

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "client", fetch = FetchType.LAZY)
	private List<Mail> listMail;

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "recipient", fetch = FetchType.LAZY)
	private List<Parcel> listReceivedParcel;

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "sender", fetch = FetchType.LAZY)
	private List<Parcel> listSentParcel;

	public Client(String lastName, String firstName, String email, String phone) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
		this.phone = phone;
	}

}

	

