package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.dao.Mail;

public interface MailRepository extends JpaRepository<Mail, Long> {

}