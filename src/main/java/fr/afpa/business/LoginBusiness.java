package fr.afpa.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.dao.Deliverer;
import fr.afpa.beans.dao.Login;
import fr.afpa.repository.dao.LoginRepository;

/**
 * Couche métier s'occupant de login
 * @author Sofiane + Cécile
 *
 */
@Service
public class LoginBusiness {
	
	@Autowired
	LoginRepository loginRepository;
	
	/**
	 * Insère un login en base de données, ainsi qu'un livreur de par la relation en cascade
	 * 
	 * @param login le login à insérer
	 * @author Cécile
	 */
	public void insert(Login login) {
		this.loginRepository.save(login);
	}
	
	/**
	 * @author Sofiane Tayeb
	 * la methode pour la vérification de connexion pour un transporteur 
	 * @param username:  le login de l'utilateur
	 * @param password mot de passe de l'utilisateur
	 * @return Deliverer: l'ojet transporteur trouvé, null sinon
	 */
	public Deliverer checkConection(String username, String password) {
		
		Login login=this.loginRepository.findByUsernameAndPassword(username, password);
		if (login!=null) {
			return login.getDeliverer();
		}
		return null;
	}
	
	/**
	 * Met à jour un login, ainsi qu'un livreur de par la relation en cascade
	 * 
	 * @param login le login à mettre à jour
	 */
	public void update(Login login) {
		this.loginRepository.saveAndFlush(login);
	}
	
	
	/**
	 * Vérifie qu'un pseudo est déjà utilisé
	 * 
	 * @param username le seudo à vérifier
	 * @return boolean : retourne true si le pseudo est déjà utilisé
	 */
	public boolean isUsernameUsed(String username) {
		return this.loginRepository.findByUsername(username) != null;
	}

}