package fr.afpa.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.dao.Client;
import fr.afpa.beans.dao.Delivery;
import fr.afpa.beans.dao.Parcel;
import fr.afpa.beans.dao.RelayPoint;
import fr.afpa.business.BarCode;
import fr.afpa.business.ClientBusiness;
import fr.afpa.business.DelivererBusiness;
import fr.afpa.business.DeliveryBusiness;
import fr.afpa.business.MailBusiness;
import fr.afpa.business.ParcelBusiness;
import fr.afpa.business.RelayPointBusiness;

/**
 * Contrôleur des colis
 * 
 * @author Sofiane Tayeb
 */

@Controller
@SessionAttributes("id")
public class ParcelController {
	@Autowired
	private DelivererBusiness delivererBusiness;	
	@Autowired
	private ParcelBusiness parcelBusiness;
	@Autowired
	private ClientBusiness clientBusiness;
	@Autowired
	private RelayPointBusiness relayPointBusiness;
	@Autowired
	private DeliveryBusiness deliveryBusiness;
	@Autowired
	private MailBusiness mailBusiness;

	/**
	 * Redirige vers la page gestion des colis
	 * @author Sofiane Tayeb
	 * @param modelView model
	 * @return String : l'url vers laquelle se rediriger
	 */
	@RequestMapping(value = "/parcelManage", method = RequestMethod.GET)
	public ModelAndView redirectToParcelManage(ModelAndView modelView) {
		modelView.setViewName("parcelManage");
		return modelView;
	}
	
	
	/**
	 * @author Djé
	 * switch vers la view listParcel avec la liste des colis
	 * 
	 * @return String : l'url vers laquelle se rediriger
	 */
	@RequestMapping(value = "/barCodeParcel", method = RequestMethod.GET)
	public ModelAndView switchToParcelList(ModelAndView model) {
		model.setViewName("listParcel");
		ArrayList<Parcel> parcels = parcelBusiness.retrieveAllParcels();
		model.addObject("listParcels", parcels);
		return model;
	}
	
	/**
	 * méthode de Redirection vers la page d'ajout de colis
	 * @author Sofiane Tayeb
	 * @return model : l'url vers laquelle se rediriger
	 */
	@RequestMapping(value = "/parcelRegister", method = RequestMethod.GET)
	public ModelAndView redirectToRegisterForm(ModelAndView modelView) {
		modelView.setViewName("parcelRegister");
		return modelView;
	}
	/**
	 * méthode de Redirection vers la page de mise à jour des colis
	 * @author Sofiane Tayeb
	 * @return Model : l'url vers laquelle se rediriger
	 */
	@RequestMapping(value = "/updateParcel", method = RequestMethod.GET)
	public ModelAndView redirectToupdateParcel(ModelAndView modelView) {
		modelView.setViewName("updateParcel");
		return modelView;
	}
	/**
	 * la méthode d'ajout des colis
	 * @author sofiane Tayeb
	 * @param mailSender: l'adresse mail de l'envoyeur du colis
	 * @param mailRecipient: l'adresse mail de receveur du colis
	 * @param price: le prix du colis
	 * @param weight: le poids du colis
	 * @param departureCity: le point relais de départ du colis
	 * @param arrivalCity: la point relais d'arrivé du colis
	 * @param intermediateCity: le point relais intermédiaire 
	 * @param idDeliverer: l'identifiant de transporteur qui enregisitre le colis
	 * @return ModelAndView : le modèle qui comportera l'url de redirection
	 */
	@RequestMapping(value = "/parcelRegister", method = RequestMethod.POST)
	public ModelAndView insertParcel(
			@RequestParam(value = "mailSender") String mailSender,
			@RequestParam(value = "mailRecipient") String mailRecipient,
			@RequestParam(value = "price") Double price,
			@RequestParam(value = "weight") Double weight,
			@RequestParam(value = "departureCity") String departureCity,
			@RequestParam(value = "arrivalCity") String arrivalCity,
			@RequestParam(value = "intermediateCity") String intermediateCity,
			@RequestParam(value = "idDeliverer") Long idDeliverer,
			ModelAndView modelView) {
		
		RelayPoint drp = relayPointBusiness.findRelayPointByName(departureCity);
		RelayPoint arp = relayPointBusiness.findRelayPointByName(arrivalCity);
		RelayPoint irp = relayPointBusiness.findRelayPointByName(intermediateCity);

		Client sender= clientBusiness.findClientByMail(mailSender);
		Client recipient= clientBusiness.findClientByMail(mailRecipient);
		
		// si le distinataire ou l'emiteur ou les points relais n'existent pas 
		// on revoie les donnée au formulaire et on demande à l'utisateur de créer les objets manquants		
		if(drp==null||arp==null||sender==null||recipient==null) {
			modelView.addObject("drp", drp);
			modelView.addObject("arp", arp);
			modelView.addObject("sender", sender);
			modelView.addObject("recipient", recipient);
			modelView.addObject("price", price);
			modelView.addObject("weight", weight);
			modelView.addObject("error", "erreur");
		
		}else {		
		Parcel parcel= new Parcel();
		ArrayList<Delivery> deliveryList = new ArrayList<Delivery>();		
		Delivery departureDelivery = new Delivery();
		departureDelivery.setStep(1);
		//departureDelivery.setDone(true);
		departureDelivery.setRelayPoint(relayPointBusiness.findRelayPointByName(departureCity));
		departureDelivery.setParcel(parcel);
		departureDelivery.setDeliverer(delivererBusiness.findById(idDeliverer).get());
		departureDelivery.setCurrentState("en cours d'acheminement");
		Delivery intermediateDelivery = new Delivery();
		intermediateDelivery.setStep(2);
		intermediateDelivery.setRelayPoint(relayPointBusiness.findRelayPointByName(intermediateCity));
		intermediateDelivery.setParcel(parcel);
		intermediateDelivery.setDeliverer(delivererBusiness.findById(idDeliverer).get());
		intermediateDelivery.setCurrentState("en cours d'acheminement");
		Delivery arrivalDelivery = new Delivery();
		arrivalDelivery.setStep(3);
		arrivalDelivery.setRelayPoint(relayPointBusiness.findRelayPointByName(arrivalCity));
		arrivalDelivery.setParcel(parcel);
		arrivalDelivery.setDeliverer(delivererBusiness.findById(idDeliverer).get());
		arrivalDelivery.setCurrentState("en cours d'acheminement");

		deliveryList.add(departureDelivery);
		deliveryList.add(intermediateDelivery);
		deliveryList.add(arrivalDelivery);
		
		parcel.setPrice(price);
		parcel.setWeight(weight);
		parcel.setCode(generatingRandomString());
		parcel.setSender(clientBusiness.findClientByMail(mailSender));		
		parcel.setRecipient(clientBusiness.findClientByMail(mailRecipient));
		parcel.setDeliveryList(deliveryList);
		parcelBusiness.insertParcel(parcel);
		
		this.mailBusiness.sendMail(this.mailBusiness.mailParcelCreated(departureDelivery), departureDelivery);
		
		modelView.addObject("parcel", parcel);
		}
		
		modelView.setViewName("parcelRegister");
		return modelView;
	}
	
	/**
	 * Cette méthode permet au transporteur de rechercher un colis par le code barre 
	 * @author sofiane
	 * @param code barre du colis
	 * @param model view définie
	 * @return redirection vers searchParcel qui contient le formulaire de changement d'etat du colis si le code est bon
	 * @return si le code n'est pas bon il reste sur la page de recherche avec un message d'erreur
	 */
	@PostMapping("/searchParcel")
	public ModelAndView searchForParcel(@RequestParam(value="codeParcel") String codeParcel, ModelAndView model) {
		Parcel parcel= parcelBusiness.findParcelByCode(codeParcel); 
		ArrayList<Delivery> listDelivery;
		if(parcel==null) {
			model.addObject("codeParcel", codeParcel);
			model.addObject("error", "colis introuvable!");			
		}else {
			listDelivery= deliveryBusiness.findDeliveryByParcel(parcel);
			model.addObject("parcel", parcel);
			model.addObject("delivery", listDelivery);
			
		}
		model.setViewName("updateParcel");
		return model;
	}	

	/**
	 * méthode de changement d'etat d'un colis à chaque point relais
	 * @author sofiane
	 * @param idParcel: l'identifiant du colis
	 * @param idDeliverer: identifiant du transporteur
	 * @param steep: l'etape en cours du colis
	 * @return ModelAndView : le modèle qui comportera les objets et url
	 */
	@RequestMapping(value = "/parcelUpdate", method = RequestMethod.POST)
	public ModelAndView updateParcel(
			@RequestParam(value = "idParcel") Long idParcel,
			@RequestParam(value = "idDeliverer") Long idDeliverer,
			@RequestParam(value = "steep") String steep,
			ModelAndView modelView ) {
			Parcel parcel = parcelBusiness.findParcelById(idParcel).get();
			RelayPoint relayPoint= relayPointBusiness.findRelayPointByName(steep);
			LocalDate localDate = LocalDate.now();
			Delivery delivery = deliveryBusiness.findDeliveryByParcelAndRelayPoint(parcel, relayPoint);
			delivery.setDone(true);
			delivery.setDeliverer(delivererBusiness.findById(idDeliverer).get());
			delivery.setDate(localDate);
			deliveryBusiness.updateDelivery(delivery);
			
			if (delivery.getStep() == 2) {
				this.mailBusiness.sendMail(this.mailBusiness.mailParcelTravel(delivery), delivery);
			} else if (delivery.getStep() == 3) {
				this.mailBusiness.sendMail(this.mailBusiness.mailParcelArrived(delivery), delivery);
			}
			
			modelView.setViewName("parcelManage");
			return modelView;		
	}	
	
	/**
	 * méthode pour la génération aléatoire du code colis
	 * @author sofiane
	 * @return un string de 10 caractères 
	 */
	public String generatingRandomString() {
	    int leftLimit = 48; 
	    int rightLimit = 122; 
	    int targetStringLength = 10;
	    Random random = new Random();

	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();

	    return generatedString;
	}
		
	/**
	 * @author Djé
	 * Permet de récupérer la liste des colis, et de leur affecter un code barre en fonction de la variable de chemin (l'id)
	 * 
	 * @param model view listParcel
	 * @param id du colis
	 * @param response
	 * @return view définie
	 * @throws IOException 
	 */
	@GetMapping("/barCodeParcel/{code}")
	public ModelAndView retrieveParcels(ModelAndView model, @PathVariable(value="code") String code, HttpServletResponse response) throws IOException {
		model.setViewName("listParcel");
		ArrayList<Parcel> parcels = parcelBusiness.retrieveAllParcels();
		response.setContentType("image/png");
		OutputStream outputStream = response.getOutputStream();
		outputStream.write(BarCode.getBarCodeImage(code, 200, 100));
		outputStream.flush();
		outputStream.close();
		model.addObject("listParcels", parcels);
		return model;
	}
	
	
	/**
	 * @author Djé
	 * Switch vers la jsp d'ajout des colis
	 * 
	 * @param model
	 * @return view parcelRegister
	 */
	@GetMapping("/addParcel")
	public ModelAndView switchToAddParcel(ModelAndView model) {
		model.setViewName("parcelRegister");
		return model;
	}
	
	/**
	 * @author Djé
	 * switch vers la jsp du menu de gestion des colis
	 * 
	 * @param model
	 * @return view parcelManage
	 */
	@GetMapping("/parcelDashboard")
	public ModelAndView switchToParcelDashboard(ModelAndView model) {
		model.setViewName("parcelManage");
		return model;
	}
	
	
}
