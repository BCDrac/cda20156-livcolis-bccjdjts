package fr.afpa.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.dao.Client;
import fr.afpa.business.ClientBusiness;

@Controller
public class ClientController {

	@Autowired
	private ClientBusiness clientBusiness;
	
	
	/**
	 * @author Djé
	 * La méthode présente récupère la liste des clients depuis la base de données 
	 * 
	 * @param model view à définir
	 * @return vers la jsp listClients, que la liste soit vide ou non
	 */
	@GetMapping("/clientList")
	public ModelAndView retrieveRooms(ModelAndView model) {
		model.setViewName("listClients");
		ArrayList<Client> listClients = clientBusiness.retrieveClients();
		model.addObject("listClients", listClients);
		return model;
	}
	
	/**
	 * @author Djé
	 * Méthode qui permet de switch de view pour amener l'utilisateur sur le formulaire de création d'un nouveau client
	 * 
	 * @param model la view en question
	 * @return switch de view
	 */
	@GetMapping("/addClient")
	public ModelAndView switchToCreateClientForm(ModelAndView model) {
		model.setViewName("formClientCreation");
		return model;
	}
	
	
	/**
	 * @author Djé
	 * Méthode qui récupère les saisies utilisateur en vue de création d'un nouveau client. Si les controles sont ok, les arguments sont passés en paramètres de la méthode createClient()
	 * pour instancier un nouveau client et l'enregistrer enfin en DB
	 * Si une exception unique est levée (même adresse mail qu'un autre en DB), on return vers le formulaire avec un message d'erreur en objet du ModelAndView
	 * 
	 * Si le client saisit des mauvais formats de téléphone, email ou noms, renvoie également vers le formulaire avec des messages d'erreurs bien ciblés via le ModelAndView grâce à la méthode listErrorsFormat()
	 * 
	 * Récupère ensuite la liste de tous les clients en DB en appelant la méthode retrieveClients()
	 * 
	 * 
	 * @param model view à définir
	 * @param lastName nom du client
	 * @param firstName prénom du client
	 * @param email du client
	 * @param phone téléphone du client
	 * @return switch vers la view définie
	 */
	@PostMapping("/addClient")
	public ModelAndView retrieveClientInputs(
			ModelAndView model, 
			@RequestParam(value="lastName") String lastName, 
			@RequestParam(value="firstName") String firstName, 
			@RequestParam(value="email") String email,
			@RequestParam(value="phone") String phone
			) {
		model.setViewName("listClients");
		String emailUnique = "L'email doit être unique";
		HashMap<String, String> formatErrors = listErrorsFormat(lastName, firstName, email, phone);
		
		
		if(isMailOk(email) && isLastNameFirstNameOk(lastName, firstName) && testPhoneOk(phone)) {
			try {
				clientBusiness.createClient(lastName, firstName, email, phone);
			} catch (Exception e) {
				model.addObject("uniqueException", emailUnique);
				model.setViewName("formClientCreation");
				return model;
			}
			
		} else {
			model.addObject("listFormatErrors", formatErrors);
			model.setViewName("formClientCreation");
			return model;
		}
		
		ArrayList<Client> listClients = clientBusiness.retrieveClients();
		model.addObject("listClients", listClients);
		
		return model;
	}
	
	
	
	
	
	/**
	 * @author Djé
	 * Méthode qui vérifie le bon format de l'adresse email
	 * 
	 * @param email : permet d'envoyer l'adresse email du client à tester au regex
	 * @return true si le format de l'email est ok, false si ne respecte pas le format
	 */
	public boolean isMailOk(String email) {
		boolean isMailValid = false;
		String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
		if(email.matches(regex)) {
			isMailValid = true;
		}
		return isMailValid;
	}
	
	/**
	 * @author Djé
	 * Méthode qui permet de rentrer en HashMap les différentes erreurs de regex possible en vue de les envoyer dans la view formClientCreation et de réutiliser son contenu.
	 * Grâce à la clé affectée à la valeur, il est plus simple de retrouver la bonne erreur dans la view formClientCreation
	 * 
	 * @param lastName passe le nom de famille en paramètre
	 * @param firstName passe le prénom en paramètre
	 * @param email passe l'email en paramètres
	 * @param phone Numéro de téléphone
	 * @return hashmap des erreurs
	 */
	public HashMap<String, String> listErrorsFormat(String lastName, String firstName, String email, String phone){
		HashMap<String, String> formatErrors = new HashMap();
		
		if (!isLastNameFirstNameOk(lastName, firstName)) {
			String namesError = "Pas d'accents";
			formatErrors.put("namesKo", namesError);
		}
		
		if (!isMailOk(email)) {
			String mailError = "Le mail ne respecte pas le bon format";
			formatErrors.put("mailKo", mailError);
		}
		
		if(!testPhoneOk(phone)) {
			String phoneError = "Le numéro de téléphone doit correspondre au format international. Commence par +...";
			formatErrors.put("phoneKo", phoneError);
		}
		
		return formatErrors;
	}
	
	
	/**
	 * @author Djé
	 * Méthode qui vérifie si la saisie des noms / prénoms matche le regex
	 *
	 * @param lastName passe le nom de famille en paramètre
	 * @param firstName passe le prénom en paramètre
	 * @return true si et seulement si les DEUX matchent avec le regex
	 */
	public boolean isLastNameFirstNameOk(String lastName, String firstName) {
		boolean isNamesValid = false;
		String regexMatcher = "[a-zA-Z]*";
		if(lastName.matches(regexMatcher) && firstName.matches(regexMatcher)) {
			isNamesValid = true;
		}
		return isNamesValid;
	}
	
	
	/**
	 * @author Djé
	 * Méthode qui vérifie si la saisie téléphone respecte le format numéro de téléphone international
	 * 
	 * @param phone passe le numéro saisi en paramètre
	 * @return true si match avec le regex
	 */
	public boolean testPhoneOk(String phone) {
		boolean isPhoneOk = false;
		String regexMatcher = "^\\+(([\\d]{1,3})||([1-][\\d]{3}))[\\d]{7,9}$";
		if(phone.matches(regexMatcher)) {
			isPhoneOk = true;
		}
		return isPhoneOk;
	}
	
	
	/**
	 * @author Djé
	 * Envoie à la couche model - classe ClientBusiness - l'id du client pour récupérer ses datas et les envoyer dans le formulaire afin qu'il soit pré-rempli,
	 * pour un meilleur User Experience. L'utilisateur n'a pas à réécrire toutes les infos s'il souhaite en modifier qu'une
	 * 
	 * 
	 * @param model récupère via la variable de chemin l'id du client
	 * @param id envoie l'id au model pour récupérer les datas du client
	 * @return switch vers view définie
	 */
	@GetMapping("/editClient/{id}")
	public ModelAndView switchToEditClient(ModelAndView model, @PathVariable(value="id") Long id) {
		model.setViewName("formEditClient");
		Optional<Client> clientById = clientBusiness.retrieveClientFromId(id);
		model.addObject("client", clientById.get());
		return model;
	}
	
	
	/**
	 * @author Djé
	 * Récupère la saisie utilisateur depuis la view formulaire formEditClient et fait les mêmes tests que pour la création client. Envoie les parammètres à la méthode editClient()
	 * de la couche model ClientBusiness
	 * 
	 * @param model view à définir
	 * @param lastName nom à modifier
	 * @param firstName prénom à modifier
	 * @param email à modifier
	 * @param phone téléphone à modifier
	 * @return switch vers la view définie
	 */
	@PostMapping("/editClient")
	public ModelAndView editClient(
			ModelAndView model,
			@RequestParam(value="id") Long id,
			@RequestParam(value="lastName") String lastName, 
			@RequestParam(value="firstName") String firstName, 
			@RequestParam(value="email") String email,
			@RequestParam(value="phone") String phone
			) {
		model.setViewName("listClients");
		String emailUnique = "L'email doit être unique";
		HashMap<String, String> formatErrors = listErrorsFormat(lastName, firstName, email, phone);
		
		
		if(isMailOk(email) && isLastNameFirstNameOk(lastName, firstName) && testPhoneOk(phone)) {
			try {
				clientBusiness.editClient(id, lastName, firstName, email, phone);
			} catch (Exception e) {
				model.addObject("uniqueException", emailUnique);
				model.setViewName("formEditClient");
				return model;
			}
		} else {
			model.addObject("listFormatErrors", formatErrors);
			model.setViewName("formEditClient");
			return model;
		}
		
		ArrayList<Client> listClients = clientBusiness.retrieveClients();
		model.addObject("listClients", listClients);
		
		return model;
	}
	
	
	/**
	 * @author Djé
	 * La méthode deleteClient() récupère la variable de chemin contenant en l'occurrence l'id du client 
	 * 
	 * @param model view qui retourne vers la liste des clients - jsp listClients -
	 * @param id du client 
	 * @return switch vers la view en question
	 */
	@GetMapping("/deleteClient/{id}")
	public ModelAndView deleteClient(ModelAndView model, @PathVariable(value="id") Long id) {
		model.setViewName("listClients");
		clientBusiness.deleteClient(id);
		ArrayList<Client> listClients = clientBusiness.retrieveClients();
		model.addObject("listClients", listClients);
		
		return model;
	}
	
	/**
	 * @author Djé
	 * Cette méthode permet à l'utilisateur de rechercher un client par email directement depuis la liste des clients dans la view listClients
	 * 
	 * @param email passé dans le champ par l'user
	 * @param model view définie
	 * @return switch vers view searchClient
	 */
	@PostMapping("/searchClient")
	public ModelAndView searchForClient(@RequestParam(value="email") String email, ModelAndView model) {
		model.setViewName("searchClient");
		String errorMessage = "Cet email n'existe pas parmi les clients. Réessayez";
		ArrayList<Client> listClients = clientBusiness.retrieveClients();
		ArrayList<Client> clientRetrieved = 
			(ArrayList<Client>) listClients
			.stream()
			.filter(o -> o.getEmail()
			.equals(email))
			.collect(Collectors.toList());
		
		if(clientRetrieved.isEmpty()) {
			model.setViewName("listClients");
			model.addObject("listClients", listClients);
			model.addObject("errorSearchEmail", errorMessage);
			return model;
		}
		
		model.addObject("clientRetrieved", clientRetrieved);
		return model;
	}
	
	/**
	 * méthode de verification d'existance d'un client par son adresse mail
     * @author Sofiane Tayeb
     * @param model view définie
     * @param email mail du client à verifier
     * @return Client : l'objet client trouvé 
     */

    @PostMapping("/verifClient")
    public ModelAndView verifClient(ModelAndView model, @RequestParam(value="email") String email) {
        Client client=clientBusiness.findClientByMail(email);
        model.addObject("client", client);
        model.setViewName("parcelRegister");
        return model;
    }
    
	/**
	 * méthode de retour au tableau de bord    
	 * @return model: la page de redirection
	 */
    @GetMapping("/backtodashboard")
    public ModelAndView switchBack(ModelAndView model) {
    	model.setViewName("delivererDashboard");
    	return model;
    }
    	
}
