<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

</head>

<jsp:include page="structure/header.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>
<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des point relais.</p>
	<TABLE BORDER="1">
		<CAPTION>Liste des Points Relais</CAPTION>
		<TR>
			<TH><a href=sortById> ID </a></TH>
			<TH><a href=sortByName> Nom </a></TH>
			<TH><a href=sortByCity> Ville </a></TH>
			<TH>Modifier</TH>
			<TH>Supprimer</TH>
		</TR>
		<c:forEach items="${ listRelayPoint }" var="relayPoint">
			<tr>
				<td><c:out value="${relayPoint.id}" /></td>
				<td><c:out value="${relayPoint.name}" /></td>
				<td><c:out value="${relayPoint.city}" /></td>
				<td><a href="${pageContext.request.contextPath}/updateRelayPoint/${relayPoint.id}"> Modifier </a></td>
				<td><a href="${pageContext.request.contextPath}/deleteRelayPoint/${relayPoint.id}"> Supprimer </a></td>
			</tr>
		</c:forEach>
	</TABLE>
	
	<button onclick="self.location.href='relayPointManage'">Retour</button>

</body>
</html>