package fr.afpa.business;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.afpa.beans.dao.Parcel;
import fr.afpa.repository.dao.ParcelRepository;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:servlet-context-test.xml"})
public class TestParcelBusiness {
	@Autowired
	ParcelBusiness parceBusiness;
	@Autowired
	ParcelRepository parcelRepository;
	
	@Test
	public void testInsertParcel() {
		Parcel parcel = new Parcel();
		parcel.setCode("VoONzNJg3a");
		parcel.setPrice(10D);
		parcel.setWeight(0.5D);		
		parceBusiness.insertParcel(parcel);
		Optional<Parcel> p = parceBusiness.findParcelById(parcel.getId());
		assertEquals("VoONzNJg3a", p.get().getCode());
		parceBusiness.deleteParcel(parcel);
	}
	@Test
	public void testDeleteParcel() {
		Parcel parcel = new Parcel();
		parcel.setCode("VoONzNJg3a");
		parcel.setPrice(10D);
		parcel.setWeight(0.5D);		
		parceBusiness.insertParcel(parcel);
		parceBusiness.deleteParcel(parcel);
		Parcel parcelTest = parceBusiness.findParcelByCode(parcel.getCode());
		assertTrue(parcelTest==null);
	}
	@Test
	public void testFindParcelByCode() {
		Parcel parcel = new Parcel();
		parcel.setCode("VoONzNJg3c");
		parcel.setPrice(10D);
		parcel.setWeight(0.5D);		
		parceBusiness.insertParcel(parcel);
		Parcel parcelTest = parceBusiness.findParcelByCode(parcel.getCode());
		assertTrue(parcelTest.getId()==parcel.getId());
		parceBusiness.deleteParcel(parcel);
	}

	@Test
	public void testFindParcelById() {
		Parcel parcel = new Parcel();
		parcel.setCode("VoONzNJg3d");
		parcel.setPrice(10D);
		parcel.setWeight(0.5D);		
		parceBusiness.insertParcel(parcel);
		Optional<Parcel> parcelCheck = parceBusiness.findParcelById(parcel.getId());
		assertTrue(parcelCheck.get().getId()==parcel.getId());	
		parceBusiness.deleteParcel(parcel);
	}

	@Test
	public void testRetrieveAllParcels() {
		
	}

}
