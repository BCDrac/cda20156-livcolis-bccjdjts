<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
		<title>Accueil</title>
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
	
	<body>
	
		<p>Vous êtes livreur ?</p>
		
		<button  onclick="self.location.href='delivererRegister'">Créer un compte</button>
		<button onclick="self.location.href='delivererAuthentication'">Connexion</button>		
		
		<p>Suivre votre colis</p>
		
		<form action="searchParcel" method="get">
			<input type="search" name="parcelCode" placeholder="Entrez le numéro de suivi"><button type="submit">Rechercher</button>
		</form>
		
	</body>
</html>