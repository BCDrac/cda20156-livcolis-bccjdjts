package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.dao.Login;

public interface LoginRepository extends JpaRepository<Login, Long> {
	public Login findByUsernameAndPassword(String username, String password);

	public Login findByUsername(String username);

}