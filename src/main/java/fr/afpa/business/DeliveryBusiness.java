package fr.afpa.business;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.dao.Delivery;
import fr.afpa.beans.dao.Parcel;
import fr.afpa.beans.dao.RelayPoint;
import fr.afpa.repository.dao.DeliveryRepository;

/**
 * Class métier de livraison  
 * @author Sofiane Tayeb
 * 
 */

@Service
public class DeliveryBusiness {
	@Autowired
	private DeliveryRepository deliveryRepository;
	
	/**
	* @author Sofiane Tayeb
	* méthode de recherche des livraison par colis
	* @param parcel le colis de recherche
	* @return listDelivery liste des livraisons trouvées
	*/
	public ArrayList<Delivery> findDeliveryByParcel(Parcel parcel) {
		return deliveryRepository.findByParcelOrderByStepAsc(parcel); 
	}
	
	/**
	 * @author Sofiane Tayeb
	 * méthode pour la recherche de livraison par id
	 * @param id l'id livaison à rechercher
	 * @return Optional Delivery : retourne l'objet Delivery (livraison) trouvé
	 */
	public Optional<Delivery> findDeliveryById(Long id) {
		return deliveryRepository.findById(id);
	}

	/**
	 * @author Djé
	 * Permet d'aller chercher et de retourner une liste de livraisons en fonction de l'objet colis passé en argument
	 * 
	 * @param parcel objet colis
	 * @return retourne une liste de livraison en fonction du colis
	 */
	public ArrayList<Delivery> retrieveDeleveryList(Parcel parcel){
		ArrayList<Delivery> delivList = (ArrayList<Delivery>) deliveryRepository.findByParcelOrderByStepAsc(parcel);
		return delivList;
	}
	
	/**
	 * @author Sofiane Tayeb
	 * méthode pour la recherche de livraison par le point relais
	 * @param relayPoint le point relais correspondant à la recherche
	 * @return Delivery: l'objet livraison trouvé, null sinon
	 */
	public Delivery findDeliveryByRelayPoint(RelayPoint relayPoint) {
		return deliveryRepository.findByRelayPoint(relayPoint);		
	}
	/**
	 * @author Sofiane Tayeb
	 * méthode de mise à jour d'une livraison
	 * @param delivery: l'bjet livraison modifié
	 */
	public void updateDelivery(Delivery delivery) {
		deliveryRepository.saveAndFlush(delivery);		
	}
	/**
	 * @author Sofiane Tayeb
	 * méthode de recherche d'une livraison par colis et point relais
	 * @param parcel: l'objet livraison correspondant à la recherche
	 * @param relayPoint: l'objet point relais correspondant à la recherche
	 * @return Delivery: l'objet livraison trouvé, null sinon
	 */
	public Delivery findDeliveryByParcelAndRelayPoint(Parcel parcel, RelayPoint relayPoint) {
		return deliveryRepository.findByParcelAndRelayPoint(parcel, relayPoint);
	}
}
