package fr.afpa.business;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.afpa.beans.dao.Client;
import fr.afpa.repository.dao.ClientRepository;
import junit.framework.TestCase;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:servlet-context-test.xml" })
public class TestClientBusiness extends TestCase {
	
	@Autowired
	ClientBusiness clientBusiness;
	@Autowired
	ClientRepository clientRepository;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	private Client clientTest; 
	
	public TestClientBusiness() {
		clientTest = new Client("Tayeb", "Sofiane", "gdgbfv.tsdfvs@gmail.com", "+33795355248");
	}

	@Test
	public void testCreateClient() {
		clientRepository.deleteAll();
		clientBusiness.createClient(clientTest.getLastName(), clientTest.getFirstName(), clientTest.getEmail(), clientTest.getPhone());
		
		Client retrieved = clientBusiness.findClientByMail(clientTest.getEmail());
		clientTest.setId(retrieved.getId());
		assertTrue(retrieved != null);
		
		Client clientFalse = clientBusiness.findClientByMail("emailWrongFormat");
		assertFalse(clientFalse != null);
		clientBusiness.deleteClient(clientTest.getId());
	}

	@Test
	public void testRetrieveClients() {
		clientRepository.deleteAll();
		clientBusiness.createClient(clientTest.getLastName(), clientTest.getFirstName(), clientTest.getEmail(), clientTest.getPhone());
		Client retrieved = clientBusiness.findClientByMail(clientTest.getEmail());
		clientTest.setId(retrieved.getId());
		ArrayList<Client> listClients2 = clientBusiness.retrieveClients();
		assertFalse(listClients2.isEmpty());
		
		clientRepository.deleteAll();
		ArrayList<Client> listClients3 = clientBusiness.retrieveClients();
		assertTrue(listClients3.isEmpty());
	}

	@Test
	public void testRetrieveClientFromId() {
		clientRepository.deleteAll();
		clientBusiness.createClient(clientTest.getLastName(), clientTest.getFirstName(), clientTest.getEmail(), clientTest.getPhone());
		Client retrieved = clientBusiness.findClientByMail(clientTest.getEmail());
		clientTest.setId(retrieved.getId());

		Optional<Client> clientById = clientBusiness.retrieveClientFromId(clientTest.getId());
		assertNotNull(clientById.get());
		
		Client wrongClient = new Client();
		wrongClient.setId(654L);
		wrongClient.setEmail("wrongMailFormat");
		assertNotSame(clientById.get(), wrongClient);
	}

	@Test
	public void testEditClient() {
		clientRepository.deleteAll();
		clientBusiness.createClient(clientTest.getLastName(), clientTest.getFirstName(), clientTest.getEmail(), clientTest.getPhone());
		Client retrieved = clientBusiness.findClientByMail(clientTest.getEmail());
		clientTest.setId(retrieved.getId());
		String firstName = "Sof";
		clientBusiness.editClient(clientTest.getId(), clientTest.getLastName(), firstName, clientTest.getEmail(), clientTest.getPhone());
		Client clientTrue2 = clientBusiness.findClientByMail(clientTest.getEmail());
		assertEquals("Sof", clientTrue2.getFirstName());
		clientRepository.deleteAll();
	}

	@Test
	public void testDeleteClient() {
		clientRepository.deleteAll();
		clientBusiness.createClient(clientTest.getLastName(), clientTest.getFirstName(), clientTest.getEmail(), clientTest.getPhone());
		Client retrieved = clientBusiness.findClientByMail(clientTest.getEmail());
		clientTest.setId(retrieved.getId());
		clientBusiness.deleteClient(clientTest.getId());
		
		Client clientDeleted = clientBusiness.findClientByMail(clientTest.getEmail());
		assertNull(clientDeleted);
	}

	@Test
	public void testFindClientByMail() {
		clientRepository.deleteAll();
		clientBusiness.createClient(clientTest.getLastName(), clientTest.getFirstName(), clientTest.getEmail(), clientTest.getPhone());
		Client retrieved = clientBusiness.findClientByMail(clientTest.getEmail());
		assertNotNull(retrieved);
		
	}

}
