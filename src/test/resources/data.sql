create table Client (id int8 not null, email varchar(255) not null, firstName varchar(255) not null, lastName varchar(255) not null, phone varchar(255), primary key (id))

create table Deliverer (id int8 not null, email varchar(255) not null, firstName varchar(255) not null, lastName varchar(255) not null, phone varchar(255), siret varchar(255) not null, primary key (id))

create table Delivery (id int8 not null, currentState varchar(255) not null, date date, done boolean default false not null, step int4 not null, id_deliverer int8, id_parcel int8, id_relayPoint int8, primary key (id))

create table Login (id int8 not null, password varchar(255) not null, username varchar(255) not null, id_deliverer int8, primary key (id))

create table Mail (id int8 not null, content text, date date not null, subject varchar(255), idClient int8, delivery_id int8, primary key (id))

create table Parcel (id int8 not null, code varchar(255) not null, delivered boolean default false not null, price float8, weight float8 not null, id_recipient int8, id_sender int8, primary key (id))

create table RelayPoint (id int8 not null, city varchar(255) not null, complement varchar(255), country varchar(255) not null, name varchar(255) not null, phone varchar(255), postalCode varchar(255) not null, streetName varchar(255) not null, streetNumber varchar(255) not null, primary key (id))

create table Schedule (id int8 not null, amClose varchar(255), amOpen varchar(255), day varchar(255) not null, pmClose varchar(255), pmOpen varchar(255), id_relay int8, primary key (id))

alter table Client drop constraint UK_f07ymtqaif0tbcawyb71l3one
alter table Client add constraint UK_f07ymtqaif0tbcawyb71l3one unique (email)
alter table Deliverer drop constraint UK_n8r6pfc5nal86xhistoob737l
alter table Deliverer add constraint UK_n8r6pfc5nal86xhistoob737l unique (email)
alter table Delivery add constraint FKqt593vyfb5n9k1ygoa8f2pon foreign key (id_deliverer) references Deliverer
alter table Delivery add constraint FKa0hjygv3is6s5kjhxxxehgghn foreign key (id_parcel) references Parcel
alter table Delivery add constraint FKivhxcfsyh5rd4xqs8d5cginvr foreign key (id_relayPoint) references RelayPoint
alter table Login add constraint FKao12b4962kgio2xa7brfr8sgo foreign key (id_deliverer) references Deliverer
alter table Mail add constraint FKd7jsujcd5gwqjlbbtxpbcmjq3 foreign key (idClient) references Client
alter table Mail add constraint FKgg2u4enrh4xr379mi6fcyka9n foreign key (delivery_id) references Delivery
alter table Parcel add constraint FKn74nt1dok5x5doaqmdfk7nh2i foreign key (id_recipient) references Client
alter table Parcel add constraint FKoi9k15il85h1g6jurggbo1xg foreign key (id_sender) references Client
alter table Schedule add constraint FKd68vdyumdonbjl81ibdpid686 foreign key (id_relay) references RelayPoint