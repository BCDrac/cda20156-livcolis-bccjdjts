package fr.afpa.beans.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Parcel {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parcel_id_gen")
	@SequenceGenerator(name="parcel_id_gen", sequenceName="sequ_parcel", allocationSize=1)
	private Long id;
	@Column(nullable = false)
	private Double weight;
	private Double price;
	@Column(nullable = false)
	private String code;
	@ColumnDefault("false")
	private boolean delivered;
	
	@OneToMany(mappedBy = "parcel", cascade = { CascadeType.ALL })
	private List<Delivery> deliveryList;
	
	@ManyToOne()
    @JoinColumn(name = "id_recipient", referencedColumnName = "id")
	private Client recipient;
	
	@ManyToOne()
    @JoinColumn(name = "id_sender", referencedColumnName = "id")
	private Client sender;
	
}