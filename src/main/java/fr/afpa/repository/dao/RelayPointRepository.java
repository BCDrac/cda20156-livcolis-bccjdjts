package fr.afpa.repository.dao;

import java.util.ArrayList;
import org.springframework.data.jpa.repository.JpaRepository;
import fr.afpa.beans.dao.RelayPoint;

public interface RelayPointRepository extends JpaRepository<RelayPoint, Long> {

	RelayPoint findByName(String name);

	
	
	
	
	

	
}