package fr.afpa.beans.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RelayPoint {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="relaypoint_id_gen")
	@SequenceGenerator(name="relaypoint_id_gen", sequenceName="sequ_relaypoint", allocationSize=1)
	private Long id;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private String streetNumber;
	@Column(nullable = false)
	private String streetName;
	private String complement;
	@Column(nullable = false)
	private String city;
	@Column(nullable = false)	
	private String postalCode;
	@Column(nullable = false)
	private String country;	
	private String phone;
	
	@OneToMany(mappedBy = "relayPoint", cascade = { CascadeType.ALL })
	private List<Delivery> deliveryList;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "relayPoint", fetch=FetchType.EAGER)
	private List<Schedule> listSchedule;
	
}