package fr.afpa.beans.dao;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Mail {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_id_gen")
	@SequenceGenerator(name = "mail_id_gen", sequenceName = "sequ_mail", allocationSize = 1)
	private Long id;
	private String subject;
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String content;
	@Column(nullable = false)
	private LocalDate date;

	@ManyToOne()
	@JoinColumn(name = "idClient", referencedColumnName = "id")
	private Client client;
	
	@OneToOne()
    private Delivery delivery;
	
}