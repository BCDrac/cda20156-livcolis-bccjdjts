package fr.afpa.beans.dao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Schedule {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "schedule_id_gen")
	@SequenceGenerator(name = "schedule_id_gen", sequenceName = "sequ_schedule", allocationSize = 1)
	private Long id;
	
	@Column(nullable = false)
	private String day;
	private String amOpen;
	private String amClose;
	private String pmOpen;
	private String pmClose;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "id_relay", referencedColumnName = "id")
	private RelayPoint relayPoint;

}