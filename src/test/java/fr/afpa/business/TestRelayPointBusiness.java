package fr.afpa.business;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.afpa.beans.dao.RelayPoint;
import fr.afpa.beans.dao.Schedule;
import fr.afpa.repository.dao.RelayPointRepository;
import junit.framework.TestCase;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:servlet-context-test.xml"})
public class TestRelayPointBusiness extends TestCase {
	
	@Autowired
	RelayPointBusiness relayPointBusiness;
	
	@Autowired
	RelayPointRepository relayPointRepository;
	

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInsertRelayPoint() {
		
		ScheduleBusiness scMonday = new ScheduleBusiness();	
		ScheduleBusiness scTuesday = new ScheduleBusiness();
		ScheduleBusiness scWednesday = new ScheduleBusiness();
		ScheduleBusiness scThursday = new ScheduleBusiness();
		ScheduleBusiness scFriday = new ScheduleBusiness();
		ScheduleBusiness scSaturday = new ScheduleBusiness();			
		ScheduleBusiness scSunday = new ScheduleBusiness();	
		
		RelayPoint rp= new RelayPoint();
		rp.setName("Jean");
		rp.setStreetNumber("20");
		rp.setStreetName("JJ Roseau");		
		rp.setCity("Inari");
		rp.setPostalCode("65000");
		rp.setCountry("Suomi");
		rp.setPhone("+33651141414");
		
		ArrayList<Schedule> schedule= new ArrayList <Schedule>();
		schedule.add(scMonday.NewInstanceSchedule("Lundi", "8", "12", "13", "16", rp ));
		schedule.add(scTuesday.NewInstanceSchedule("Mardi", "8", "12", "13", "16", rp ));
		schedule.add(scWednesday.NewInstanceSchedule("Mercredi", "8", "12", "13", "16", rp ));
		schedule.add(scThursday.NewInstanceSchedule("Jeudi", "8", "12", "13", "16", rp ));
		schedule.add(scFriday.NewInstanceSchedule("Vendredi", "8", "12", "13", "16", rp ));
		schedule.add(scSaturday.NewInstanceSchedule("Samedi", "8", "12", "13", "16", rp ));
		schedule.add(scSunday.NewInstanceSchedule("Dimanche", "8", "12", "13", "16", rp ));
		
		rp.setListSchedule(schedule);	
		
		this.relayPointBusiness.insertRelayPoint(rp);
		

		Optional<RelayPoint> RelayPointCheck = this.relayPointBusiness.retrieveRelayPointById(rp.getId());
		assertEquals("Jean", RelayPointCheck.get().getName());
		
		this.relayPointBusiness.deleteRelayPoint(rp.getId());
		
	}

	@Test
	public void testEditRelayPoint() {
	}

	@Test
	public void testFindRelayPointByName() {
		
		ScheduleBusiness scMonday = new ScheduleBusiness();	
		ScheduleBusiness scTuesday = new ScheduleBusiness();
		ScheduleBusiness scWednesday = new ScheduleBusiness();
		ScheduleBusiness scThursday = new ScheduleBusiness();
		ScheduleBusiness scFriday = new ScheduleBusiness();
		ScheduleBusiness scSaturday = new ScheduleBusiness();			
		ScheduleBusiness scSunday = new ScheduleBusiness();	
		
		RelayPoint rp= new RelayPoint();
		rp.setName("Jean");
		rp.setStreetNumber("20");
		rp.setStreetName("JJ Roseau");		
		rp.setCity("Inari");
		rp.setPostalCode("65000");
		rp.setCountry("Suomi");
		rp.setPhone("+33651141414");
		
		ArrayList<Schedule> schedule= new ArrayList <Schedule>();
		schedule.add(scMonday.NewInstanceSchedule("Lundi", "8", "12", "13", "16", rp ));
		schedule.add(scTuesday.NewInstanceSchedule("Mardi", "8", "12", "13", "16", rp ));
		schedule.add(scWednesday.NewInstanceSchedule("Mercredi", "8", "12", "13", "16", rp ));
		schedule.add(scThursday.NewInstanceSchedule("Jeudi", "8", "12", "13", "16", rp ));
		schedule.add(scFriday.NewInstanceSchedule("Vendredi", "8", "12", "13", "16", rp ));
		schedule.add(scSaturday.NewInstanceSchedule("Samedi", "8", "12", "13", "16", rp ));
		schedule.add(scSunday.NewInstanceSchedule("Dimanche", "8", "12", "13", "16", rp ));
		
		rp.setListSchedule(schedule);	
		
		this.relayPointBusiness.insertRelayPoint(rp);
		
		RelayPoint RelayPointCheck = this.relayPointBusiness.findRelayPointByName(rp.getName());
		assertEquals("Jean", RelayPointCheck.getName());		
		
		this.relayPointBusiness.deleteRelayPoint(rp.getId());
		
	}

	@Test
	public void testRetrieveListRelayPoint() {
		
		ScheduleBusiness scMonday = new ScheduleBusiness();	
		ScheduleBusiness scTuesday = new ScheduleBusiness();
		ScheduleBusiness scWednesday = new ScheduleBusiness();
		ScheduleBusiness scThursday = new ScheduleBusiness();
		ScheduleBusiness scFriday = new ScheduleBusiness();
		ScheduleBusiness scSaturday = new ScheduleBusiness();			
		ScheduleBusiness scSunday = new ScheduleBusiness();	
		
		RelayPoint rp= new RelayPoint();
		rp.setName("Jean");
		rp.setStreetNumber("20");
		rp.setStreetName("JJ Roseau");		
		rp.setCity("Inari");
		rp.setPostalCode("65000");
		rp.setCountry("Suomi");
		rp.setPhone("+33651141414");
		
		ArrayList<Schedule> schedule= new ArrayList <Schedule>();
		schedule.add(scMonday.NewInstanceSchedule("Lundi", "8", "12", "13", "16", rp ));
		schedule.add(scTuesday.NewInstanceSchedule("Mardi", "8", "12", "13", "16", rp ));
		schedule.add(scWednesday.NewInstanceSchedule("Mercredi", "8", "12", "13", "16", rp ));
		schedule.add(scThursday.NewInstanceSchedule("Jeudi", "8", "12", "13", "16", rp ));
		schedule.add(scFriday.NewInstanceSchedule("Vendredi", "8", "12", "13", "16", rp ));
		schedule.add(scSaturday.NewInstanceSchedule("Samedi", "8", "12", "13", "16", rp ));
		schedule.add(scSunday.NewInstanceSchedule("Dimanche", "8", "12", "13", "16", rp ));
		
		rp.setListSchedule(schedule);			
		this.relayPointBusiness.insertRelayPoint(rp);
		
		
		ArrayList<RelayPoint> ListCheck = new ArrayList<RelayPoint>();		
		ListCheck = this.relayPointBusiness.retrieveListRelayPoint(ListCheck);	
		
		assertTrue( ListCheck!=null);
		
		this.relayPointBusiness.deleteRelayPoint(rp.getId());
		
	
	}

	@Test
	public void testRetrieveListRelayPointSortedById() {
	
	}

	@Test
	public void testRetrieveListRelayPointSortedByName() {
	
	}

	@Test
	public void testRetrieveListRelayPointSortedByCity() {
	
	}

	@Test
	public void testRetrieveRelayPointById() {
		
		ScheduleBusiness scMonday = new ScheduleBusiness();	
		ScheduleBusiness scTuesday = new ScheduleBusiness();
		ScheduleBusiness scWednesday = new ScheduleBusiness();
		ScheduleBusiness scThursday = new ScheduleBusiness();
		ScheduleBusiness scFriday = new ScheduleBusiness();
		ScheduleBusiness scSaturday = new ScheduleBusiness();			
		ScheduleBusiness scSunday = new ScheduleBusiness();	
		
		RelayPoint rp= new RelayPoint();
		rp.setName("Jean");
		rp.setStreetNumber("20");
		rp.setStreetName("JJ Roseau");		
		rp.setCity("Inari");
		rp.setPostalCode("65000");
		rp.setCountry("Suomi");
		rp.setPhone("+33651141414");
		
		ArrayList<Schedule> schedule= new ArrayList <Schedule>();
		schedule.add(scMonday.NewInstanceSchedule("Lundi", "8", "12", "13", "16", rp ));
		schedule.add(scTuesday.NewInstanceSchedule("Mardi", "8", "12", "13", "16", rp ));
		schedule.add(scWednesday.NewInstanceSchedule("Mercredi", "8", "12", "13", "16", rp ));
		schedule.add(scThursday.NewInstanceSchedule("Jeudi", "8", "12", "13", "16", rp ));
		schedule.add(scFriday.NewInstanceSchedule("Vendredi", "8", "12", "13", "16", rp ));
		schedule.add(scSaturday.NewInstanceSchedule("Samedi", "8", "12", "13", "16", rp ));
		schedule.add(scSunday.NewInstanceSchedule("Dimanche", "8", "12", "13", "16", rp ));
		
		rp.setListSchedule(schedule);			
		this.relayPointBusiness.insertRelayPoint(rp);		

		Optional<RelayPoint> RelayPointCheck = this.relayPointBusiness.retrieveRelayPointById(rp.getId());
		assertEquals(rp.getId(), RelayPointCheck.get().getId());		
		
		this.relayPointBusiness.deleteRelayPoint(rp.getId());
	
	}

	@Test
	public void testDeleteRelayPoint() {
		ScheduleBusiness scMonday = new ScheduleBusiness();	
		ScheduleBusiness scTuesday = new ScheduleBusiness();
		ScheduleBusiness scWednesday = new ScheduleBusiness();
		ScheduleBusiness scThursday = new ScheduleBusiness();
		ScheduleBusiness scFriday = new ScheduleBusiness();
		ScheduleBusiness scSaturday = new ScheduleBusiness();			
		ScheduleBusiness scSunday = new ScheduleBusiness();	
		
		RelayPoint rp= new RelayPoint();
		rp.setName("Jean");
		rp.setStreetNumber("20");
		rp.setStreetName("JJ Roseau");		
		rp.setCity("Inari");
		rp.setPostalCode("65000");
		rp.setCountry("Suomi");
		rp.setPhone("+33651141414");
		
		ArrayList<Schedule> schedule= new ArrayList <Schedule>();
		schedule.add(scMonday.NewInstanceSchedule("Lundi", "8", "12", "13", "16", rp ));
		schedule.add(scTuesday.NewInstanceSchedule("Mardi", "8", "12", "13", "16", rp ));
		schedule.add(scWednesday.NewInstanceSchedule("Mercredi", "8", "12", "13", "16", rp ));
		schedule.add(scThursday.NewInstanceSchedule("Jeudi", "8", "12", "13", "16", rp ));
		schedule.add(scFriday.NewInstanceSchedule("Vendredi", "8", "12", "13", "16", rp ));
		schedule.add(scSaturday.NewInstanceSchedule("Samedi", "8", "12", "13", "16", rp ));
		schedule.add(scSunday.NewInstanceSchedule("Dimanche", "8", "12", "13", "16", rp ));
		
		rp.setListSchedule(schedule);	
		this.relayPointBusiness.insertRelayPoint(rp);
		
		this.relayPointBusiness.deleteRelayPoint(rp.getId());
		
		assertNull( this.relayPointBusiness.findRelayPointByName("Jean"));
		
	}

}
