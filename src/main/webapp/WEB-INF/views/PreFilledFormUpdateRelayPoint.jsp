<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulaire pré-rempli de modification de Point-Relais </title>
</head>

<jsp:include page="structure/header.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>

	<form action="${pageContext.request.contextPath}/editRelayPoint" method="post">

		ID: <input type="text" name="id" value='${ UpdateRelayPoint.id }' required><br /> 
		Nom : <input type="text" name="name" value='${ UpdateRelayPoint.name }' required><br /> 
		Numéro de rue : <input type="number" name="streetNumber" value='${ UpdateRelayPoint.streetNumber }'  required><br>
		Nom de rue : <input	type="text" name="streetName" value='${ UpdateRelayPoint.streetName }'  required><br> 
		Complément d'adresse :<input type="text" name="complement" value='${ UpdateRelayPoint.complement }' ><br> 
		Ville :	<input type="text" name="city" value='${ UpdateRelayPoint.city }'  required><br> 
		Code Postal : <input type="text" name="postalCode"  value='${ UpdateRelayPoint.postalCode }' required><br> 
		Pays : <input type="text" name="country" value='${ UpdateRelayPoint.country }' required><br> 
		Téléphone : <input type="tel" name="phone" value='${ UpdateRelayPoint.phone }'  required><br>


		<TABLE BORDER="1">
			<CAPTION>Horaires</CAPTION>
			<TR>
				<TH>Jour</TH>
				<TH>Ouverture Matin</TH>
				<TH>Fermeture Matin</TH>
				<TH>Ouverture Après-Midi</TH>
				<TH>Fermeture Après-Midi</TH>
			</TR>
			<tr>
				<td>Lundi</td>
				<td><input type="text" name="MondayAmOpen" value='${UpdateRelayPointSchedule[0].amOpen}' required></td>
				<td><input type="text" name="MondayAmClose" value='${UpdateRelayPointSchedule[0].amClose }' required></td>
				<td><input type="text" name="MondayPmOpen" value='${UpdateRelayPointSchedule[0].pmOpen }' required></td>
				<td><input type="text" name="MondayPmClose" value='${ UpdateRelayPointSchedule[0].pmClose }' required></td>
			</tr>
			<tr>
				<td>Mardi</td>
				<td><input type="text" name="TuesdayAmOpen"  value='${UpdateRelayPointSchedule[1].amOpen}' required></td>
				<td><input type="text" name="TuesdayAmClose"  value='${ UpdateRelayPointSchedule[1].amClose }' required></td>
				<td><input type="text" name="TuesdayPmOpen"  value='${UpdateRelayPointSchedule[1].pmOpen}' required></td>
				<td><input type="text" name="TuesdayPmClose"  value='${  UpdateRelayPointSchedule[1].pmClose}' required></td>
			</tr>
			<tr>
				<td>Mercredi</td>
				<td><input type="text" name="WednesdayAmOpen" value='${UpdateRelayPointSchedule[2].amOpen}' required></td>
				<td><input type="text" name="WednesdayAmClose" value='${ UpdateRelayPointSchedule[2].amClose }' required></td>
				<td><input type="text" name="WednesdayPmOpen" value='${UpdateRelayPointSchedule[2].pmOpen }' required></td>
				<td><input type="text" name="WednesdayPmClose" value='${  UpdateRelayPointSchedule[2].pmClose }' required></td>
			<tr>
				<td>Jeudi</td>
				<td><input type="text" name="ThursdayAmOpen" value='${UpdateRelayPointSchedule[3].amOpen}' required></td>
				<td><input type="text" name="ThursdayAmClose" value='${UpdateRelayPointSchedule[3].amClose}' required></td>
				<td><input type="text" name="ThursdayPmOpen" value='${UpdateRelayPointSchedule[3].pmOpen }' required></td>
				<td><input type="text" name="ThursdayPmClose" value='${  UpdateRelayPointSchedule[3].pmClose}' required></td>
			<tr>
				<td>Vendredi</td>
				<td><input type="text" name="FridayAmOpen" value='${UpdateRelayPointSchedule[4].amOpen}' required></td>
				<td><input type="text" name="FridayAmClose" value='${UpdateRelayPointSchedule[4].amClose}' required></td>
				<td><input type="text" name="FridayPmOpen" value='${UpdateRelayPointSchedule[4].pmOpen }' required></td>
				<td><input type="text" name="FridayPmClose" value='${ UpdateRelayPointSchedule[4].pmClose}' required></td>
			<tr>
				<td>Samedi</td>
				<td><input type="text" name="SaturdayAmOpen" value='${UpdateRelayPointSchedule[5].amOpen}' required></td>
				<td><input type="text" name="SaturdayAmClose" value='${UpdateRelayPointSchedule[5].amClose }' required></td>
				<td><input type="text" name="SaturdayPmOpen" value='${ UpdateRelayPointSchedule[5].pmOpen}' required></td>
				<td><input type="text" name="SaturdayPmClose" value='${ UpdateRelayPointSchedule[5].pmClose}' required></td>
			</tr>
			<tr>
				<td>Dimanche</td>
				<td><input type="text" name="SundayAmOpen" value='${UpdateRelayPointSchedule[6].amOpen}' required></td>
				<td><input type="text" name="SundayAmClose" value='${UpdateRelayPointSchedule[6].amClose}' required></td>
				<td><input type="text" name="SundayPmOpen" value='${UpdateRelayPointSchedule[6].pmOpen }' required></td>
				<td><input type="text" name="SundayPmClose" value='${ UpdateRelayPointSchedule[6].pmClose}' required></td>
			</tr>



		</TABLE>
		<button type="submit">Envoyer</button>
	</form>

	<button onclick="self.location.href='${pageContext.request.contextPath}/relayPointManage'">Retour</button>

</body>
</html>