package fr.afpa.business;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import org.apache.taglibs.standard.lang.jstl.AndOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import fr.afpa.beans.dao.RelayPoint;
import fr.afpa.repository.dao.RelayPointRepository;

@Service
public class RelayPointBusiness {

	@Autowired
	private RelayPointRepository relayPointRepository;
	Boolean bool = true;

	/**
	 * Insère un objet RelayPoint en base de données
	 * 
	 * @author Ju
	 * @param relayPoint Objet RelayPoint
	 */
	public void insertRelayPoint(RelayPoint relayPoint) {
		this.relayPointRepository.save(relayPoint);
	}
	
	/**
	 * Edite un objet RelayPoint en base de données
	 * 
	 * @author Ju
	 * @param relayPoint Objet RelayPoint
	 */
	public void editRelayPoint(RelayPoint relayPoint) {
		this.relayPointRepository.saveAndFlush(relayPoint);
	}

	/**
	 * methode de recherche d'un point relais par nom
	 * 
	 * @author sofiane
	 * @param name: le nom de point relais à rechercher
	 * @return relayPoint:  Objet RelayPoint trouvé, un null sinon
	 */
	public RelayPoint findRelayPointByName(String name) {

		return this.relayPointRepository.findByName(name);
	}

	/**
	 * Recupère la liste des points relais
	 * 
	 * @author Ju
	 * @param listRelayPoint Objet RelayPoint
	 * @return listRelayPoint : ArrayList de RelayPoint
	 */
	public ArrayList<RelayPoint> retrieveListRelayPoint(ArrayList<RelayPoint> listRelayPoint) {
		listRelayPoint = (ArrayList<RelayPoint>) relayPointRepository.findAll();
		
		return listRelayPoint;
	}

	/**
	 * Recupère la liste des points relais et les organise par ID
	 * 
	 * @author Ju
	 * @param listRelayPoint Objet RelayPoint
	 * @return listRelayPoint : : ArrayList de RelayPoint
	 */
	public Object retrieveListRelayPointSortedById(ArrayList<RelayPoint> listRelayPoint) {		
		listRelayPoint = (ArrayList<RelayPoint>) relayPointRepository.findAll(Sort.by("id").ascending());

		return listRelayPoint;
	}

	/**
	 * Recupère la liste des points relais et les organise par nom
	 * 
	 * @author Ju
	 * @param listRelayPoint Objet RelayPoint
	 * @return listRelayPoint : : ArrayList de RelayPoint
	 */
	public ArrayList<RelayPoint> retrieveListRelayPointSortedByName(ArrayList<RelayPoint> listRelayPoint) {
		listRelayPoint = (ArrayList<RelayPoint>) relayPointRepository.findAll(Sort.by("name").ascending());

		return listRelayPoint;
	}

	
	/**
	 * Recupère la liste des points relais et les organise par ville
	 * 
	 * @author Ju
	 * @param listRelayPoint Objet RelayPoint
	 * @return listRelayPoint : : ArrayList de RelayPoint
	 */
	public ArrayList<RelayPoint> retrieveListRelayPointSortedByCity(ArrayList<RelayPoint> listRelayPoint) {
		listRelayPoint = (ArrayList<RelayPoint>) relayPointRepository.findAll(Sort.by("city").ascending());

		return listRelayPoint;
	}
	
	/**
	 * Recupère un objet point Relais en effecuant une recherche par ID, passé en paramètre
	 * 
	 * @author Ju
	 * @param id Long id
	 * @return rp : Optional RelayPoint
	 */
	public Optional<RelayPoint> retrieveRelayPointById (Long id) {		
		Optional<RelayPoint> rp = relayPointRepository.findById(id);
				
		return rp;	
		
	}
	
	
/** 
 * Supprime un point Relais en effectuant une recherche par ID, passé en paramètre
 * 
 *  @author Ju
 * @param id Long id
 */
	public void deleteRelayPoint(Long id) {
		relayPointRepository.deleteById(id);
		
	}
	

}
