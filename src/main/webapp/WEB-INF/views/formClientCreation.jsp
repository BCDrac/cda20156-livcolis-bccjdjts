<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Création client</title>
</head>

	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>

<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des utilisateur.</p>	<h1>Ajouter un client</h1>
	<br>
	<br>
	<div>
		<form action="${pageContext.request.contextPath}/addClient" method="post">
			Nom : <input type="text" name="lastName" required /> <br>
			Prénom : <input type="text" name="firstName" required />
			<c:out value="${ listFormatErrors['namesKo'] }"></c:out> <br> 
			Email : <input type="text" name="email" required /> <br> 
			<c:out value="${ listFormatErrors['mailKo'] }"></c:out> <br>
			<c:out value="${ uniqueException }"></c:out><br>
			Téléphone : <input type="text" name="phone" required /> 
			<c:out value="${ listFormatErrors['phoneKo'] }"></c:out> <br> <br>
			<button type="submit">Valider</button>
		</form>
	</div>
	<a href="clientList">Retour</a>
</body>
</html>