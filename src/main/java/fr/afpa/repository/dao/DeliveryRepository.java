package fr.afpa.repository.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.dao.Deliverer;
import fr.afpa.beans.dao.Delivery;
import fr.afpa.beans.dao.Parcel;
import fr.afpa.beans.dao.RelayPoint;

public interface DeliveryRepository extends JpaRepository<Delivery, Long>{

	
	/**
	 * @author Djé
	 * Permet de récupérer une liste de livraison par la FK de parcel (colis)
	 * 
	 * @param parcelid le colis passé en paramètre, recherché par le client
	 * @return la liste des livraisons par colis
	 */
	public List<Delivery> findByParcel(
			Long parcelId);
	
	ArrayList<Delivery> findByParcelOrderByStepAsc(Parcel parcel);

	Delivery findByRelayPoint(RelayPoint relayPoint);

	Delivery findByParcelAndRelayPoint(Parcel parcel, RelayPoint relayPoint);
	
	public Delivery findByDelivererAndParcelAndRelayPoint(Deliverer deliverer, Parcel parcel, RelayPoint relayPoint);
}