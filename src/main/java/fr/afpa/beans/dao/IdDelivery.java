package fr.afpa.beans.dao;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
@Embeddable
public class IdDelivery implements Serializable {
	
//	private Long idRelayPoint;
//	private Long idDeliverer;
//	private Long idParcel;

}