package fr.afpa.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.dao.Delivery;
import fr.afpa.beans.dao.Parcel;
import fr.afpa.business.DeliveryBusiness;
import fr.afpa.business.ParcelBusiness;

@Controller
public class DeliveryController {

	@Autowired
	private DeliveryBusiness deliveryBusiness;
	
	@Autowired ParcelBusiness parcelBusiness;
	
	
	/**
	 * @author Djé
	 * Récupère le code colis passé dans le champs de recherche depuis la view home puis envoie 
	 * 
	 * @param model définit la view suivante en fonction des conditions
	 * @param code code du colis
	 * @return la view trackPackage
	 */
	@GetMapping("/searchParcel")
	public ModelAndView packageTracking(ModelAndView model, @RequestParam(value="parcelCode") String code) {
		model.setViewName("trackPackage");
		//récupère un colis par le biais du code colis
		Optional<Parcel> parcel = parcelBusiness.retrieveParcelByCode(code);
		model.addObject("parcel", parcel.get());
		if (!parcel.isPresent()) {
			return model;
		}
		ArrayList<Delivery> deliveries = deliveryBusiness.retrieveDeleveryList(parcel.get());
		model.addObject("deliveryList", deliveries);
		return model;
	}
	
	
	
	
}
