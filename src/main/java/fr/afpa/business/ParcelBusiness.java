package fr.afpa.business;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.dao.Parcel;
import fr.afpa.repository.dao.ParcelRepository;

/**
* classe de gestion des colis
* 
* @author Sofiane Tayeb
*/

@Service
public class ParcelBusiness {
	@Autowired
	ParcelRepository parcelRepository;
	
	/**
	* methode d'insertion des colis 
	* @author Sofiane Tayeb
	* @param parcel le colis à inserer dans la bdd 
	*/
	public void insertParcel(Parcel parcel) {
		parcelRepository.save(parcel);
	}
	/**
	* methode pour supprimer un colis
	* @author Sofiane Tayeb
	* @param parcel:  l'objet colis à supprimer 
	*/
	public void deleteParcel(Parcel parcel) {
		parcelRepository.delete(parcel);
	}
	/**
	* methode de recherche des colis par code barre
	* @author Sofiane Tayeb
	* @param codeParcel le code barre 
	* @return Parcel: l'objet colis trouvé, un null sinon
	*/
	public Parcel findParcelByCode(String codeParcel) {
		return parcelRepository.findByCode(codeParcel);
		
	}
	
	/**
	* methode de recherche des colis par id
	* @author Sofiane Tayeb
	* @param idParcel: l'identifiant du colis à rechercher
	* @return Optional Parcel: l'objet colis trouvé, un null sinon
	*/
	public Optional<Parcel> findParcelById(Long idParcel) {
		return parcelRepository.findById(idParcel);		
	}
	
	/**
	 * @author Djé
	 * La méthode présente récupère le colis par le code passé en paramètre. La méthode ofNullable héritée de Optional permet de retourner un Optional empty qui sert de contrôle 
	 * de nullité dans le DeliveryController
	 * 
	 * @param code code du colis
	 * @return retourne un seul client si le code du colis matche avec un des codes de la liste des colis en DB
	 */
	public Optional<Parcel> retrieveParcelByCode(String code){
		Optional<Parcel> parcel = Optional.ofNullable(parcelRepository.findByCode(code));
		return parcel;
	}
	
	/**
	 * @author Djé
	 * Permet de récupérer la liste des colis en vue de les envoyer à la view listParcel
	 * 
	 * @return liste des colis
	 */
	public ArrayList<Parcel> retrieveAllParcels(){
		ArrayList<Parcel> parcels = (ArrayList<Parcel>) parcelRepository.findAll();
		return parcels;
	}
	
	
}
