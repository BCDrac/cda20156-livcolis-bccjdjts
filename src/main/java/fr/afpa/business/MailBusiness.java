package fr.afpa.business;

import java.time.LocalDate;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.dao.Delivery;
import fr.afpa.beans.dao.Mail;
import fr.afpa.repository.dao.MailRepository;

@Service
public class MailBusiness {

	@Autowired
	private MailRepository mailRepository;
	
	
	/**
	 * Envoie  un mail à un utilisateur d'après une livraison
	 * 
	 * @param mail le mail à envoyer
	 * @param delivery la livraison concernée
	 */
	public void sendMail(Mail mail, Delivery delivery) {
		
		Properties smtpProperties = new Properties();

		smtpProperties.put("mail.smtp.auth", "true");
		smtpProperties.put("mail.smtp.starttls.enable", "true");
		smtpProperties.put("mail.smtp.ssl.trust", "*");
		smtpProperties.put("mail.smtp.host", "smtp-mail.outlook.com");	
		smtpProperties.put("mail.smtp.port", "587");
		
		String myEmail = "afpa-cda-test@hotmail.com";
		String myPassword = "123afpacda456";
		
		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myEmail, myPassword);
			}
		};
		
		Session sessionMail = Session.getInstance(smtpProperties, auth);
		
		try {
			Message content = new MimeMessage(sessionMail);
			content.setFrom(new InternetAddress(myEmail));
			content.setRecipients(Message.RecipientType.TO, InternetAddress.parse(
				delivery.getParcel().getRecipient().getEmail()
				)
			);	
			content.setSubject(mail.getSubject());
			content.setText(mail.getContent());
			
			Transport.send(content);
			
			this.mailRepository.save(mail);
			
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Crée un mail pour l'envoi du colis
	 * 
	 * @param delivery la livraison concernée par le mail
	 * @return Mail : renvoie un objet Mail
	 */
	public Mail mailParcelCreated(Delivery delivery) {
		Mail mail = new Mail();
		
		mail.setClient(delivery.getParcel().getRecipient());
		mail.setDelivery(delivery);
		mail.setDate(LocalDate.now());
		mail.setSubject("Expédition de votre colis.");
		mail.setContent("Bonjour " 
			+ delivery.getParcel().getRecipient().getFirstName() + " "
			+ delivery.getParcel().getRecipient().getLastName() + ",\n"
			+ "Votre colis envoyé par " 
			+ delivery.getParcel().getSender().getFirstName() + " "
			+ delivery.getParcel().getSender().getLastName() + " "
			+ "vient d'être expédié et vous parviendra dans les jours à venir.\n"
			+ "Vous pouvez suivre l'avancée de votre livraison sur le site en saisissant "
			+ "dans la barre de recherche le code suivant : " + delivery.getParcel().getCode() +".\n"
			+ "Cordialement,\n"
			+ "L'équipe Colis-Trot'.");
		return mail;
	}
	
	
	/**
	 * Crée un mail pour la mise à jour du trajet du colis
	 * 
	 * @param delivery la livraison concernée par le mail
	 * @return Mail : renvoie un objet Mail
	 */
	public Mail mailParcelTravel(Delivery delivery) {
		Mail mail = new Mail();
		
		mail.setClient(delivery.getParcel().getRecipient());
		mail.setDelivery(delivery);
		mail.setDate(LocalDate.now());
		mail.setSubject("Point sur le trajet de votre colis.");
		mail.setContent("Bonjour " 
			+ delivery.getParcel().getRecipient().getFirstName() + " "
			+ delivery.getParcel().getRecipient().getLastName() + ",\n"
			+ "Votre colis est arrivé au point-relais " + delivery.getRelayPoint().getName() + ".\n"
			+ "Vous pouvez suivre l'avancée de votre livraison sur le site en saisissant "
			+ "dans la barre de recherche le code suivant : " + delivery.getParcel().getCode() +".\n"
			+ "Cordialement,\n"
			+ "L'équipe Colis-Trot'.");
		return mail;
	}
	
	
	/**
	 * Crée un mail lors du stade final de la livraison du colis
	 * 
	 * @param delivery la livraison concernée par le mail
	 * @return Mail : renvoie un objet Mail
	 */
	public Mail mailParcelArrived(Delivery delivery) {
		Mail mail = new Mail();
		
		mail.setClient(delivery.getParcel().getRecipient());
		mail.setDelivery(delivery);
		mail.setDate(LocalDate.now());
		mail.setSubject("Votre colis est arrivé.");
		mail.setContent("Bonjour " 
			+ delivery.getParcel().getRecipient().getFirstName() + " "
			+ delivery.getParcel().getRecipient().getLastName() + ",\n"
			+ "Votre colis est arrivé au point-relais " + delivery.getRelayPoint().getName() + " " 
			+ "à l'adresse suivante : " 
			+ delivery.getRelayPoint().getStreetNumber() + " " + delivery.getRelayPoint().getStreetName() + " "
			+ delivery.getRelayPoint().getPostalCode() + " " + delivery.getRelayPoint().getCity() + ", " 
			+ delivery.getRelayPoint().getCountry() + ".\n"
			+ "Vous pouvez désormais le récupérer.\n"
			+ "Nous vous remercions d'avoir fait confiance à nos services.\n"
			+ "Cordialement,\n"
			+ "L'équipe Colis-Trot'.");
		return mail;
	}
	
}