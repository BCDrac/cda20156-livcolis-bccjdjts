package fr.afpa.business;

import org.springframework.stereotype.Service;

import fr.afpa.beans.dao.RelayPoint;
import fr.afpa.beans.dao.Schedule;

@Service
public class ScheduleBusiness {
	
	/**
	 * Instancie un nouvel objet schedule à partir des informations passées en paramètre
	 * 
	 * @author : Ju
	 * @param day jour	
	 * @param amOpen horaire d'ouverture du matin
	 * @param amClose horaire de fermeture du matin
	 * @param pmOpen horaire d'ouverture de l'aprem
	 * @param pmClose horaire de fermeture de l'aprem
	 * @param relayPoint point relais
	 * @return objet Schedule
	 */
	public Schedule NewInstanceSchedule(String day, String amOpen, String amClose, String pmOpen, String pmClose, RelayPoint relayPoint) {
		
		Schedule sc = new Schedule();
		sc.setDay(day);
		sc.setAmOpen(amOpen);
		sc.setAmClose(amClose);
		sc.setPmOpen(pmOpen);
		sc.setPmClose(pmClose);
		sc.setRelayPoint(relayPoint);
		
		return sc;
		
	}

}
