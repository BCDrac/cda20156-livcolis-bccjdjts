<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
		<title>Tableau de bord</title>
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
	
	<body>
		<c:set scope="session" var="deliverer" value="${deliverer}" />
		
		<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" /> , bienvenue sur votre tableau de bord.</p>
		
		
		<button onclick="self.location.href='disconnect'">Se déconnecter</button>
		
		 <%-- Vérification de la présence d'un objet utilisateur en session --%>
                <c:if test="${!empty sessionScope.deliverer.id}">
                    <%-- Si l'utilisateur existe en session, alors on affiche son adresse email. --%>
                    <p class="succes">Vous êtes connecté(e) avec l'adresse : ${sessionScope.deliverer.email}</p>
                </c:if>
		<h1>Tableau de bord</h1>
		
		<div>
			<figure>
				<a href="parcelManage">
					<img src="${pageContext.request.contextPath}/resources/img/parcelManage.png"
		         	alt="Gestion de colis">
		       	 </a>
	  			<figcaption>Gérer les colis</figcaption>
 			</figure>
		
 			<figure>
 				<a href="relayPointManage">
					<img src="${pageContext.request.contextPath}/resources/img/warehouse.png"
		         	alt="Gestion de point relais">
	        	</a>
	  			<figcaption>Gérer les points de relais</figcaption>
 			</figure>
		
 			<figure>
 				<a href="clientList">
					<img src="${pageContext.request.contextPath}/resources/img/userGroup.png"
		         	alt="Gestion des utilisateurs">
	        	</a>
	  			<figcaption>Gérer les utilisateurs</figcaption>
 			</figure>

			<figure>
				<a href="delivererAccount">
					<img src="${pageContext.request.contextPath}/resources/img/account.png"
					alt="Voir mon compte">
				</a>
				<figcaption>Voir mon compte</figcaption>
			</figure>
		</div>
	
	</body>
</html>