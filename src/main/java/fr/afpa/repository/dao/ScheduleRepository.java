package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.dao.Schedule;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

}