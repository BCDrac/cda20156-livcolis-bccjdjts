<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>ajouter un colis</title>
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
	
	<body>

		<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" />, bienvenue sur la gestion des colis.</p>
	
			<form action="${pageContext.request.contextPath}/parcelRegister" method="post">
				<input type="text" name="idDeliverer" value="${deliverer.id}" required hidden>
				<label>Email expéditeur : </label> <input type="text" name="mailSender" value="${sender.email}" placeholder="ex: t.s@mail.com" required>
				<c:if test="${!empty error and empty sender}" ><span style="color:red;">client introuvable, cliquer<a href="${pageContext.request.contextPath}/addClient"> ici </a>pour l'ajouter</c:if> </span> <br> 
				<label>Email destinataire : </label> <input type="text" name="mailRecipient" value="${recipient.email}" placeholder="ex: t.s@mail.com" required>
				<c:if test="${!empty error and empty recipient}" ><span style="color:red;">client introuvable, cliquer<a href="${pageContext.request.contextPath}/addClient"> ici </a>pour l'ajouter </c:if></span> <br> 
				<label>Prix : </label> <input type="text" name="price"  value="${price}" placeholder="tapez le prix" required><br/>	
				<label>Poids : </label> <input type="text" name="weight" value="${weight}" placeholder="tapez le poids" required><br/>
				<label>point relais de départ : </label> <input type="text" name="departureCity" value="${drp.name}" placeholder="ex: lille" required>
				<c:if test="${!empty error and empty drp}" ><span style="color:red;">point relais introuvable, cliquer<a href="${pageContext.request.contextPath}/relayPointRegister"> ici </a>pour l'ajouter</c:if></span> <br> 
				<label>point relais d'arrivée : </label> <input type="text" name="arrivalCity" value="${arp.name}" placeholder="ex: paris" required>	
				<c:if test="${!empty error and empty arp}" ><span style="color:red;">point relais introuvable, cliquer<a href="${pageContext.request.contextPath}/relayPointRegister"> ici </a>pour l'ajouter</c:if></span> <br> 
				<label>Ajouter des points relais intermédiaires : </label> 
				<input type="text" name="intermediateCity" placeholder="" required ><br/>	
				<button type="submit" >Envoyer</button>			
			</form>
	<br/>
	<button onclick="self.location.href='parcelManage'">Retour</button>
</body>
</html>