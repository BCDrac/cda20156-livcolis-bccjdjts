<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Mise a jour colis</title>
</head>

	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>

<body>
	<p>
		Bonjour
		<c:out value="${deliverer.lastName} ${ deliverer.firstName}" />
		, bienvenue sur la gestion des colis.
	</p>
	<c:choose>
			<c:when test="${ empty parcel  }">
				<form action="${pageContext.request.contextPath}/searchParcel"
					method="post">
					<input type="text" placeholder="entrez le code barre"
						name="codeParcel" value="${parcel.code}" required />
					<button type="submit">Valider</button>
					<br />
					<c:if test="${ !empty error  }">
						<span>${error} </span>
					</c:if>
					</br>
				</form>
			</c:when>
			<c:when test="${!empty parcel}">
				<form action="${pageContext.request.contextPath}/parcelUpdate" method="post">
					<input type="text" name="idParcel" value="${parcel.id}" required hidden> 
					<input type="text" name="idDeliverer" value="${deliverer.id}" required hidden> 
					<label>code du colis : </label><input type="text" name="codeParcel" value="${parcel.code}" required disabled> <br />
						<label>point relais : </label> 
					<c:forEach items="${delivery}" var="d" varStatus="loopstatus">
						<c:if test="${ d.done==true  }">
							<input type="radio" value="${d.relayPoint.name}" checked disabled />${d.relayPoint.name}
						</c:if>
						<c:if test="${ d.done==false  }">
								<input type="radio" name="steep" value="${d.relayPoint.name}" />${d.relayPoint.name}
						</c:if>
					</c:forEach>
						<button type="submit">Valider</button>
				</form><br />
				</c:when>
	</c:choose> <br />
	<button onclick="self.location.href='parcelManage'">Retour</button>
</body>
</html>