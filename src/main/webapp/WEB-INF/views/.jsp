<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>S'inscrire</title>
	</head>
	<body>
			
	<form action="insertDeliverer" method="post">
	
		Nom : <input type="text" name="lastName"><br/>
		Prénom : <input type="text" name="firstName"><br/>
		Email : <input type="email" name="email"><br/>
		Téléphone : <input type="number" name="phone"><br/>
		<br/>
		Login : <input type="text" name="login"><br/>
		Mot de passe : <input type="password" name="password"><br/>
		Réécrivez votre mot de passe : <input type="password" name="passwordCheck"><br/>
		<br/>
		Siret : <input type="text" name="siret"><br/>
		
		<button type="submit" >Envoyer</button>
	</form>
	
	<button  onclick="self.location.href='home'">Retour</button>
	
	</body>
</html>