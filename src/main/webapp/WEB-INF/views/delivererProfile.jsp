<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Mon compte</title>
	</head>
	
	<jsp:include page="structure/header.jsp"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
	
	<body>
		<p>Bonjour <c:out value="${deliverer.lastName} ${ deliverer.firstName}" /> !</p>
		<button onclick="self.location.href='disconnect'">Se déconnecter</button>
		
		<c:choose>
		
			<c:when test="${update}">
				<form action="${pageContext.request.contextPath}/updateAccount" method="post">
					<label>ID : </label> <input type="text" name="id" value="${deliverer.id}" readonly=readonly hidden="true"><br/>
					
					<c:out value="${errorList['lastName']}"/><br/>
					<label>Nom : </label> <input type="text" name="lastName" value="${deliverer.lastName}" required><br/>
					
					<c:out value="${errorList['firstName']}"/><br/>
					<label>Prénom : </label> <input type="text" name="firstName" value="${deliverer.firstName}" required><br/>
					
					<c:out value="${errorList['email']}"/><br/>
					<label>Email : </label> <input type="email" name="email" value="${deliverer.email}" readonly=readonly required><br/>
					
					<c:out value="${errorList['phone']}"/><br/>
					<label>Téléphone : </label> <input type="tel" name="phone" maxlength="13" pattern="^\+(([\d]{1,3})|([1-][\d]{3}))[\d]{7,9}$" value="${deliverer.phone}" required><br/>
					
					<c:out value="${errorList['siret']}"/><br/>
					<label>Siret : </label> <input type="text" name="siret" maxlength="14" pattern="^[0-9]{14}$" value="${deliverer.siret}" required><br/>

					<br/>

					<c:out value="${errorList['username']}"/><br/>
					<label>Login : </label> <input type="text" name="username" value="${deliverer.login.username}" readonly=readonly required><br/>
					
					<c:out value="${errorList['password']}"/><br/>
					<label>Mot de passe : </label> <input type="password" name="password"><br/>
					
					<c:out value="${errorList['passwordCheck']}"/><br/>
					<label>Réécrivez votre mot de passe : </label> <input type="password" name="passwordCheck"><br/>
					<br/>
					<button type="submit" >Valider les informations</button>
				</form>
			</c:when>
			
			<c:when test="${!update}">	
				<div>
					<p>Mon compte</p>
					
					<p><span>Nom : <c:out value="${deliverer.lastName}"></c:out></span></p>
					<p><span>Prénom : <c:out value="${deliverer.firstName}"></c:out></span></p>
					<p><span>Email : <c:out value="${deliverer.email}"></c:out></span></p>
					<p><span>Téléphone : <c:out value="${deliverer.phone}"></c:out></span></p>
					<p><span>SIRET : <c:out value="${deliverer.siret}"></c:out></span></p>
					<p><span>Login : <c:out value="${deliverer.login.username}"></c:out></span></p>
				</div>
				<button onclick="self.location.href='editAccount'">Modifier mes informations</button>
			</c:when>

		</c:choose>
		
		<br/>
		<button onclick="self.location.href='delivererDashboard'">Retour</button>
	
	</body>
</html>