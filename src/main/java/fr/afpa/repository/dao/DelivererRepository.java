package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.beans.dao.Deliverer;

@Repository
public interface DelivererRepository extends JpaRepository<Deliverer, Long> {

	public Deliverer findByEmail(String email);	

}