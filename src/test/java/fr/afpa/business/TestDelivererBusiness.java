package fr.afpa.business;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.afpa.beans.dao.Deliverer;
import fr.afpa.repository.dao.DelivererRepository;
import junit.framework.TestCase;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:servlet-context-test.xml"})
public class TestDelivererBusiness extends TestCase{

	@Autowired
	private DelivererBusiness delivererBusiness;
	@Autowired
	private DelivererRepository delivererRepository;
	
	@Test
	public void testInsertDeliverer() {
		
		Deliverer deliverer = new Deliverer(); 
		deliverer.setLastName("Insert");
		deliverer.setFirstName("In");
		deliverer.setEmail("mail@insert.fr");
		deliverer.setSiret("06512480397405");
		deliverer.setPhone("+5132049875");

		this.delivererBusiness.insertDeliverer(deliverer);
		
		Optional<Deliverer> delivererCheck = this.delivererBusiness.findById(deliverer.getId());
		assertEquals("Insert", delivererCheck.get().getLastName());
		
		this.delivererBusiness.deleteDeliverer(deliverer);
	}


	@Test
	public void testIsEmailUsed() {

		Deliverer deliverer = new Deliverer();
		deliverer.setLastName("Mail");
		deliverer.setFirstName("Ma");
		deliverer.setEmail("mail@true.fr");
		deliverer.setSiret("06512480397405");
		deliverer.setPhone("+5132049875");

		this.delivererBusiness.insertDeliverer(deliverer);
		
		assertTrue(this.delivererBusiness.isEmailUsed(deliverer.getEmail()));
		
		deliverer.setEmail("mail@false.fr");
		assertFalse(this.delivererBusiness.isEmailUsed(deliverer.getEmail()));
		
		this.delivererBusiness.deleteDeliverer(deliverer);
	}

	
	@Test
	@Order(3)
	public void testFindById() {
		
		Deliverer deliverer = new Deliverer();
		deliverer.setLastName("lastName");
		deliverer.setFirstName("firstName");
		deliverer.setEmail("mail@findid.fr");
		deliverer.setSiret("06512480397405");
		deliverer.setPhone("+5132049875");
		
		this.delivererBusiness.insertDeliverer(deliverer);
		
		Optional<Deliverer> delivererCheck = this.delivererBusiness.findById(deliverer.getId());
		
		assertTrue(delivererCheck.get().getId() == deliverer.getId());
		
		this.delivererBusiness.deleteDeliverer(deliverer);
	}

	
	@Test
	@Order(4)
	public void testUpdateDeliverer() {
		
		Deliverer deliverer = new Deliverer();
		deliverer.setLastName("lastName");
		deliverer.setFirstName("firstName");
		deliverer.setEmail("mail@changeit.fr");
		deliverer.setSiret("06512480397405");
		deliverer.setPhone("+5132049875");
		
		this.delivererBusiness.insertDeliverer(deliverer);
		Deliverer deliverer2 = this.delivererBusiness.findByEmail(deliverer.getEmail());
		
		deliverer2.setEmail("changedEmail");
		
		this.delivererBusiness.updateDeliverer(deliverer2);
		Deliverer delivererCheck = this.delivererBusiness.findByEmail(deliverer2.getEmail());
		
		assertEquals("changedEmail", delivererCheck.getEmail());
		
		this.delivererBusiness.deleteDeliverer(deliverer);
		this.delivererBusiness.deleteDeliverer(deliverer2);
	}
	
	@Test
	@Order(5)
	public void testDeleteDeliverer() {
		
		Deliverer deliverer = new Deliverer();
		deliverer.setLastName("lastName");
		deliverer.setFirstName("firstName");
		deliverer.setEmail("mail@delete.fr");
		deliverer.setSiret("06512480397405");
		deliverer.setPhone("+5132049875");
		
		this.delivererBusiness.insertDeliverer(deliverer);
		this.delivererBusiness.deleteDeliverer(deliverer);
		
		Deliverer delivererCheck = this.delivererBusiness.findByEmail(deliverer.getEmail());
		
		assertTrue(delivererCheck == null);
	}
}